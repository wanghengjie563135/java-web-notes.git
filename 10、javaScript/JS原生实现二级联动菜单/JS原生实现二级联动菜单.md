# JS原生实现二级联动菜单（市/区县）

# 一、下拉列表二级/三级联动示意图

![](https://i.loli.net/2021/10/20/mEZyrsBiek1oYfg.png)

![](image/image_1.png)

## 二、js中操作下拉列表的相关属性

1. selObj.value 获取下拉列表中选中项的值 
2. selObj.options 下拉列表中所有选项构成的数组
3.  selObj.selectedIndex 选中选项的下标
4. selObj.options[selObj.selectedIndex].text 选中选项的文本

案例：

```html
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>下拉列表标签属性的案例</title>
    <script>
        //js代码
        function test1() {
              //获取select标签对象
            var city = document.getElementById("city");
             //下拉列表对象 options 获取下拉列表所有的option标签对象
            var options = city.options;
            //获取下拉列表有几个数据
            alert(options.length);
            //下拉列表对象，selectedIndex 获取下拉列表选中的option下标
            alert(city.selectedIndex);
            //下拉列表对象.value 获取下拉列表中选中的option的value值
            alert(city.value);
        }
    </script>
</head>
<body>
城市：
<select onchange="test1()" id="city">
    <option value="tj">天津</option>
    <option value="bj"> 北京</option>
    <option value="sh">上海</option>
    <option value="cq">重庆</option>
</select>
</body>
</html>
```


## 三、 JS简单实现二级联动菜单

> 实现思路：
       事件源头：城市对应的下拉列表
      事件属性：选中的值改变  onchange
      事件监听：根据选择城市展示县区信息


```java
核心:监听函数的实现——>根据选中城市展示县区信息

1. json格式的数据准备：key-value
var city={
  tj:["北辰区","西青区","武清区","和平区","滨海新区"],
  zy:["红花港区","播州区"],
  bj:["海淀区","朝阳区","东城区"]
  zz:["二七区","金水区","中原区"],
}
2. 创建监听函数test();
   （1）每次选择前先去清空县区下拉列表的内容 document.getElementBYId("qx").innerText="";
    (2)获取城市的信息document.getElementBYId("city").value;
    (3)获取区县的值（两种方法） city.["tj"];
    (4)遍历区县的信息for(inner in 区县的值qus)
       a、为区县的值封装成文本节点
       b、创建option标签对象 document.crementElement("option");
       c、将文本节点追加到option中 option.appendChild(文本节点);
       d、将option封装到select select.appendChild(option);
3. 初始化方法，刷新页面时，自动选择第一个县区信息
function  init(){
            //初始化方法，刷新页面时，自动选择第一个县区信息
            test1();
        }
```


#### 1、html代码：

1、首先应该添加两个下拉列表并设置id属性来方便操作：

```html
<body onload="init();">
城市：
<select onchange="test1()" id="city">
    <option value="tj">天津</option>
    <option value="zy"> 遵义</option>
    <option value="bj">北京</option>
    <option value="zz">郑州</option>
</select>
区/县：
<select id="qx">
</select>
</body>
```


#### 2、javaScript代码

js先提前准备区县的数据：

```java
  //数据准备
        var city = {
            tj: ["北辰区", "西青区", "武清区", "和平区", "滨海新区"],
            zy: ["红花港区", "播州区"],
            bj: ["海淀区", "朝阳区", "东城区"],
            zz: ["二七区", "金水区", "中原区"],
        }
```


实现二级联动的方法：test1(),

实现初始化的方法，让打开网页时区县有初始数据init();

```html
 <script>
    
        //js代码
        function test1() {
            //每次选择前先去清空县区下拉列表的内容
            var xqSelect= document.getElementById("qx");
            xqSelect.innerText="";

            //1获取city的名字（value）
            var selectValue = document.getElementById("city").value;
            //2.从city对象中获取指定城市对应的区县信息
            //json根据键获取值的两种方法：key.value || key[value]
            var xqElement = city[selectValue];
            //3.获取到区县后的进行遍
            for (var i=0;i<xqElement.length;i++) {
            //4.获取当前元素封装成文本节点
                var text = document.createTextNode( xqElement[i]);
            //5.创建option对象
                var option = document.createElement("option");
            //6.将区、县的数据添加到option标签对象中
                option.appendChild(text);
            //7.将option放到select标签中
                xqSelect.appendChild(option);
            }
        }
        function  init(){
            //初始化方法，刷新页面时，自动选择第一个县区信息
            test1();
        }
    </script>

```



