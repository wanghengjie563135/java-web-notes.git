# Day1 基本语法、事件

# 一、JavaScript简介

JavaScript（原名：ECMAScript）:网景 （Netscape）公司开发,基于浏览器的，解释型的脚本语言（写在Html中）

- 作用

```java
1. 表单验证
2. 页面特性
3. 操作页面元素
```


注意：JS是弱类型语言；严格区分大小写，**且存在浏览器差异** 

- 一门客户端脚本语言

```java
* 运行在客户端浏览器中的。每一个浏览器都有JavaScript的解析引擎
* 脚本语言：不需要编译，直接就可以被浏览器解析执行了
```


- 功能：

```markdown
 # jsp: java服务页面 实现页面的动态效果 动的是数据 ${requestScope.name}
 # js:              实现页面的动态效果 动的是页面结构（标签）
 可以来增强用户和html页面的交互过程，可以来控制html元素，
    让页面有一些动态的效果，增强用户的体验。操作Html的页面结构
```


- JavaScript发展史：

```java
1. 1992年，Nombase公司，开发出第一门客户端脚本语言，专门用于表单的校验。命名为 ： C--  ，
后来更名为：ScriptEase

2. 1995年，Netscape(网景)公司，开发了一门客户端脚本语言：LiveScript。
后来，请来SUN公司的专家，修改LiveScript，命名为JavaScript

3. 1996年，微软抄袭JavaScript开发出JScript语言

4. 1997年，ECMA(欧洲计算机制造商协会)，制定出客户端脚本语言的标准：ECMAScript，
就是统一了所有客户端脚本语言的编码方式。

* JavaScript = ECMAScript + JavaScript自己特有的东西(BOM+DOM)

```


- 注意事项：

```markdown
# js和java没有关系 独立的编程语言
# js是一种解释型编程语言
     编译型：运行效率高，不能跨平台  源码--》字节码 运行的字节码 C C++
     解释型： 逐行解释运行，运行效率低，运行的是源代码，能跨平台 js
     先编译再解释： 运行效率较高 可跨平台 java

# js的解释器---浏览器
  WebKit 内核系列：chrome、360..【标准w3c组织】
  IE内核系列：IE浏览器（IE 6.0版本以前）
  js浏览器差异：解析过程中也存在浏览器差异的

# js代码核心功能是操作页面结构，不能独立运行，依托与HTML运行
```


# 二、JavaScript使用

```java
1. 基本语法：
    1. 与html结合方式
      1. 内部JS：
        * 定义<script>，标签体内容就是js代码
      2. 外部JS：
        * 定义<script>，通过src属性引入外部的js文件

      * 注意：
        1. <script>可以定义在html页面的任何地方。但是定义的位置会影响执行顺序。
        2. <script>可以定义多个。
    2. 注释
      1. 单行注释：//注释内容
      2. 多行注释：/*注释内容*/
```


### 1、引入js方式

```java
在html代码中通过<script>标签嵌套js代码
<script type="text/javascript">
//js语句
</script>

注意;在开发过程中通常会将这段代码放在<head></head>标签中
<head>
<script type="text/javascript">
//js语句
</script>
</head>

```


### 2、js中输出

```HTML
1. 向浏览器窗口中输出HelloWord
  <script type="text/javascript">
    document.write("<h1>HelloWord</h1>");
  </script>
2.一警告框的形式打印输出结果
 <script type="text/javascript">
  window.alert("HelloWord");
  </script>
3.向浏览器控制台输出结果
<script type="text/javascript">
    console.info("HelloWord");
</script>

```


·案例·：

```html
 <script type="text/javascript">
        <!--书写js代码-->
        //在HTML中页面输出
        document.write("helloWord")
        //浏览器弹框输出 参数：输出的内容，阻断后续程序运行
        alert("helloWord");
        //浏览器控制台输出 快捷键：F12
        /**
         * console.log()、console.info()、console.debug()的作用都是在浏览器控制台打印信息的。
         使用最多的是console.log()。
         console.info()和console.debug()本质上与console.log()没有区别。
         是console.log()的另一个名字而以，可以理解为它们三个其实就是同一个，只不过名字不一样。
         其中，console.info()打印出的信息，在控制显示的时候信息前面会出现一个小图标，
         而且谷歌浏览器和opera不支持console.debug()。
         */
        console.log("helloWord");
        console.info("Hello World!")
    </script>
```


### **3、变量** 

```java
1.js是一种弱类型类型语言
java--是一种强类型语言（变量的类型和值的类型一致）
int a=3; String str="whj";
js--是一种弱类型的语言（只有值类型，没有变量类型）
var a=3; a="whj"

*强类型：在开辟变量存储空间时，定义了空间将来存储的数据的数据类型。
        只能存储固定类型的数据
* 弱类型：在开辟变量存储空间时，不定义空间将来的存储数据类型，
         可以存放任意类型的数据。
2.js的变量定义
变量：一小块存储数据的内存空间
变量可可以多次重名声明定义赋值，值以最后一次有值的赋值为准
var a=4; a=false; a="hehe";

typeof运算符：获取变量的类型。
* 注：null运算后得到的是object

```


**4、数据类型** 

```java
1.基本数据类型
数字类型--不分小数和整数
字符串类型--不分字符和字符串，用单引号或者双引号声明定义 “hjehe”
布尔类型--true|false 非0|0（false） 非空|null(false)

*1. number：数字。 整数/小数/NaN(not a number 一个不是数字的数字类型)
*2. string：字符串。不区分字符和字符串，不区分单双引号 字符串  "abc" "a" 'abc'
*3. boolean: true和false
*4. null：一个对象为空的占位符
*5. undefined：未定义。如果一个变量没有给初始化值，则会被默认赋值为undefined

3.引用、复合型数据类型
数组 日期 对象 函数类型

4.JS特殊语法：
      1. 语句以;结尾，如果一行只有一条语句则 ;可以省略 (不建议)
      2. 变量的定义使用var关键字，也可以不使用
            * 用： 定义的变量是局部变量
            * 不用：定义的变量是全局变量(不建议)
5、特殊类型：
   NAN not a number 进行了不正常的数字运算  “whj/2”=NAN
   undefined 变量声明后未赋值直接使用 var a; alert(a);
   null 变量手工赋值为null,函数返回值为null

6、关键字 tepyof 查看当前变量的实际数据类型 使用方式
    var a="10";
    var b=10;
    alert(a=b);//true
    alert(typeof a);//string
    alert(typeof b)//number
    alert(typeof a= typeof b);//true
```


### 5、运算符

```java
1.数学运算符：+ - / % *
注意：只有能计算出结果 ，js就会自动类型转换切换运算出结果
2、判断相等性 ：== ===
==:比较两种是否相等
var a=3; var b="3" alter(a==b)-->true
===:比较值和类型是否相等
var a=3; var b="3" alter(a===b)-->false

例：
<script type="text/javascript">
   a=3; b="3"; window.alert(a===b);
</script>

3.typeof 判断变量的类型
4.三元运算符
        ? : 表达式
        var a = 3;
            var b = 4;
    
            var c = a > b ? 1:0;
        * 语法：
          * 表达式? 值1:值2;
          * 判断表达式的值，如果是true则取值1，如果是false则取值2；

```


### 6、JavaScript流程控制

```java
1、判断语句 ---与java一样
if（）{}
if（）else{}
switch(a) case  java中a的值：byte,short,int,char,String(1.7以后)  js:没要求

2、循环控制：
for（var i=0;i++）{}
while(){}
do()while()

```


![](https://i.loli.net/2021/10/18/vKAYUpWFNedQ1lk.png)

### 7、练习：99乘法表

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
  <script>
<!--    99乘法表-->
    document.write("<table align='center'>");

//控制行数
for (var i = 1; i <=9 ; i++) {
  document.write("<tr>");
  for (var j = 1; j <=i ; j++) {

    document.write("<td>");
    document.write(i+"*"+j+"="+(i*j)+"&nbsp;&nbsp;&nbsp;");
    document.write("</td>");
  }
  //换行输出
  document.write("</tr>");
}
document.write("</table>");
  </script>
</head>
```


# 三、自定义函数【重点】

概念：特点功能的代码块，可以反复调用

- 在js例函数被定义为一种数据类型，一个函数也可以称为“一个函数类型的值”，保存之这个值的变量就是函数名

- 定义函数

```java
1.使用关键字function定义函数
function 函数名(参数名，....){函数体}

function add(a,b){return a+b;}

调用：函数名+参数表 ：
    add(10,20);

2.隐式声明(匿名)定义
语法：var函数名=function(函数名){函数体}
  用function定义一个函数，将其看做是一个值，将这个值赋值给变量。
  例如：var add=function add(a,b){return a+b;}
  使用：变量名（实参）
  add(5,2);
  
3.函数类型值可以在变量间赋值
  var f1=function(){return "hehe"}
  var f2=f1;-->将f1这个函数赋值给f2,f2也是一个函数
  var f3=f1():-->将f1这个函数的结果赋值给f3，f3就是字符串“hehe”
```


4、函数可以作为函数的参数【常用】

![](https://i.loli.net/2021/10/18/4lixt5JX8Tnk1hC.png)

```java
实现方法二：
test(function (i,j)){
  return i+j;
}
```


- 案例：将java中的排序放到前端使用

![](https://i.loli.net/2021/10/18/OxXlARQ34JYSrI2.png)

5、函数可以不按照规定传递参数

```Java
  function add(i+j){
  return i+j;
  }
  var result1=add(1);  //1+undefined=NaN
  var result1=add(1,2);//3
  var result1=add(1,2,3,4);//3(只取前两位)
  
  * 注意：js中的函数可以不按照指定参数去传参，可能会出问题
```


### 1、内置数组对象Arguments[重点]

注意事项：

```java
js里没有“方法重载”，调用函数时可以传入多余or少于形参数量的实参。
js将所有的实参默认保存在一个叫做arguments的数组里。
注：arguments（争吵）只能在函数内部使用

function add(i,j){
  if(typeof i=="number"&&typeof="number"){
       if(arguments.length==2){
           return i+j;
        }else{
           alert("类型有误，请重新输入");
        }
        }else{
             alert("参数个数有错，请重新输入");
        }
  }
  
  *arguments作用：增强函数的健壮性
```


### 2、内置函数

```Java
1.parseInt函数：把str转化成number整数
document.write(parseInt("123")+"<br/>");//123
document.write(parseInt("1.23")+"<br/>")//1
document.write(parseInt("1a23")+"<br/>");//1
document.write(parseInt("a123")+"<hr/>");//NaN

2.parseFloat函数：把str转化成number小数
document.write(parseFloat("3.14")+"<br/>")//3.14
document.write(parseFloat("a123")+"<br/>");//NaN（not 啊number）
document.write(parseFloat("1a.23")+"<br/>")//1
```


# 四、对象

### 1、对象类型

```Markdown
# js语言是一门类（类似）的面向对象的编程语言，js没有类的概念

#1、在js里只有对象，没有类，任意两个对象都不一样

# 2.定义一个对象
  语法：var obj={属性名：属性值，属性名：属性值，.......};
#3.访问对象的属性
（1）访问一个属性：对象名  或者 对象名{"属性名"}

（2）遍历所有属性： 对象名.属性名 或者 对象名["属性名"]
     循环每一次执行，都会从对象中获取一个属性值，赋值给指定的变量名。
    属性特点：
         1. 方法定义是，形参的类型不用写,返回值类型也不写。
         2. 方法是一个对象，如果定义名称相同的方法，会覆盖
         3. 在JS中，方法的调用只与方法的名称有关，和参数列表无关
         4. 在方法声明中有一个隐藏的内置对象（数组），arguments,封装所有的实际

```


### 2、自定义对象：自己定义的对象

#### （1）new Object();方式创建对象 【了解】

```Java
  <script type="text/javascript">
        //创建一个学生对象
        var student = new Object();
        //属性+方法 属性的定义 对象名.属性名
        student.id = 1;
        student.name = '王恒杰';
        student.age = 21;
        //获取属性值
        alert("id是：" + student.id);
        alert("名字是：" + student.name);
        alert("年龄是：" + student.age);
        student.getId=function(){
            return this.id;
        }
       student.setId=function(id){
            this.id=id
        }
       student.getName=function(){
            return this.name;
        }
        student.setName=function(name){
            this.name=name;
        }
        student.getAge=function(){
            return this.age;
        }
        student.setAge=function(age){
            this.age=age;
        }
        alert(student.getName());
        student.setName("王恒杰whj");
        alert(student.getName());
    </script>

```

#### （2）同构系统与异构系统

```markdown
# 异构系统通过XML，JSON，字符串进行多语言的通讯，因为这些都是规定好的，各个语言中使用都是一样的。例如JavaScript和Java通过Json进行数据交换，JavaScript利用eval()函数，而Java有jackson,json-lib等类库来进行帮忙解析。

# 不使用xml的原因：JSON 相对于XML要轻量，XML就比较笨重了，所以现在很多数据传输都在逐渐转为使用JSON来作为传输数据的方式。（2）同构系统与异构系统
```

- 同构系统

![](https://i.loli.net/2021/10/18/CFg7jmBUfOylJST.png)

- 异构系统

![](https://i.loli.net/2021/10/18/Ht5Sb4Ax8pN9Vd2.png)

#### （3）json形式对象创建

是一种数据交换格式，本质就是一个特殊形式的字符串 —— json串<br />   特殊形式：{key：value ， key1 ： value1....}

```java
  <script type="text/javascript">
        //创json建一个对象
        var student = {
            id: 1, name: 'whj', age: 18, getAge: function () {
                return this.age;
            }, setAge: function (age) {
                this.age = age;
            },
            //有play方法（play方法返回一个字符串值）
            play:function(){return "hehe"}
        }
        //访问属性
        alert(student.id);
        alert(student.name);
        alert(student.age);
        //访问方法（模拟）
        alert(student.getAge());
        student.setAge(58);
        alert(student.getAge());
        alert(student.play)
    </script>
    
 *异常：Assiguments patterns must be on the left side of assignment
       分配模式必须在分配模式的左侧，写：不要写=
 *typeof运算符：获取变量的类型。
```


### 3、内置对象：js中准备好的

#### （1）数组【重点】

```markdown
# java中数组的特点：定长,元素类型相同  
# js中数组的特点：js里的数组长度不固定，可以任意扩展，数据可以是任意类型。 

```


```html
<script type="text/javascript">
1.创建数组的两种方式
  //创建数组
  //1.隐式，Array数组对象 new Array();  ing[] arr1=new int[10]
   var arr1=new Array();
  //2.显示，显示初始化数组  var arr2=[元素1，元素2];
    var arr2=[1,2,3,4]

2.操作数组
    var arr1=new Array();
   //操作数组元素 下标
   arr1[0]='whj';
   arr1[1]='yfj';
   //有arr1[2]这种方式
   arr1[3]='whx';
   //获取数组的元素值
  alert(arr1[1]);
  
3.数组遍历的两种方式  
  //数组遍历
  /**
   * 下标遍历
   * @type {number[]}
   */
  for (var i = 0; i < arr1.length; i++) {
      alert(arr1[i]);
  }
  /**
   * foreach遍历
   * foreach遍历 java: for(当前元素类型 名字，遍历的数组或集合)
   *               js: for(当前元素的下标 in 遍历的数组)
   * @type {number[]}
   */
  for (index in arr1) {
    alert(arr1[index])
  }
</script>
```


- 相关属性方法

```java
 var arr2 = [1, 2, 3, 4]
    //压栈（添加数组）
    arr2.push();
    //弹栈（将末尾元素移除）
    arr2.pop();
    
1.数组对象.sort()--->对数组里的元素按照自然顺序升序排列
2.数组对象.push()--->在数组的末尾插入一个元素
3.数组对象.pop--->删除数组末尾的最后一个元素，且数组长度减1
4.数组对象.join(参数)-->将数组中的元素按照指定的分隔符拼接为字符串
5.delete 数组对象[下标]--->删除数组指定位置元素，且数组长度不变
```


#### （2）字符串

```html
  <script type="text/javascript">
    //创建字符串
    var string1 = new String("whj");
    var string2 = "whj";
    alert(string1 + "=====" + string2)
    string2.length;
  </script>
```


#### （3）日期

```html
   <script type="text/javascript">
   //日期
   let date = new Date();
   alert(date)
   //获取年
    alert(date.getFullYear());
   // 月 系统是0--11
   alert(date.getMonth()+1);
   // 日
   alert(date.getDate());
   // 时
   alert(date.getHours());
   // 分
   alert(date.getSeconds());
   // 秒
   alert(date.getMinutes());
   //周
   alert(date.getDay());
   //毫秒
   alert(date.getMilliseconds());
  </script>
```


####   (4)Math

```Java
//随机数 返回0-1之间的随机数
        let number = Math.random();
        alert(number)
        
             1. 创建：
                * 特点：Math对象不用创建，直接使用。  Math.方法名();

            2. 方法：
                random():返回 0 ~ 1 之间的随机数。 含0不含1
                ceil(x)：对数进行上舍入。
                floor(x)：对数进行下舍入。
                round(x)：把数四舍五入为最接近的整数。
            3. 属性：
                PI
 5. RegExp：正则表达式对象
      1. 正则表达式：定义字符串的组成规则。
        1. 单个字符:[]
          如： [a] [ab] [a-zA-Z0-9_]
          * 特殊符号代表特殊含义的单个字符:
            \d:单个数字字符 [0-9]
            \w:单个单词字符[a-zA-Z0-9_]
        2. 量词符号：
          ?：表示出现0次或1次
          *：表示出现0次或多次
          +：出现1次或多次
          {m,n}:表示 m<= 数量 <= n
            * m如果缺省： {,n}:最多n次
            * n如果缺省：{m,} 最少m次
        3. 开始结束符号
          * ^:开始
          * $:结束
      2. 正则对象：
        1. 创建
          1. var reg = new RegExp("正则表达式");
          2. var reg = /正则表达式/;
        2. 方法  
          1. test(参数):验证指定的字符串是否符合正则定义的规范 

```


#### （5）RegExp：正则表达式对象

```java
      1. 正则表达式：定义字符串的组成规则。
        1. 单个字符:[]
          如： [a] [ab] [a-zA-Z0-9_]
          * 特殊符号代表特殊含义的单个字符:
            \d:单个数字字符 [0-9]
            \w:单个单词字符[a-zA-Z0-9_]
        2. 量词符号：
          ?：表示出现0次或1次
          *：表示出现0次或多次
          +：出现1次或多次
          {m,n}:表示 m<= 数量 <= n
            * m如果缺省： {,n}:最多n次
            * n如果缺省：{m,} 最少m次
        3. 开始结束符号
          * ^:开始
          * $:结束
      2. 正则对象：
        1. 创建
          1. var reg = new RegExp("正则表达式");
          2. var reg = /正则表达式/;
        2. 方法  
          1. test(参数):验证指定的字符串是否符合正则定义的规范 
```


#### （6）Global

```jaav

      1. 特点：全局对象，这个Global中封装的方法不需要对象就可以直接调用。  方法名();
      2. 方法：
          encodeURI():url编码
          decodeURI():url解码

          encodeURIComponent():url编码,编码的字符更多
          decodeURIComponent():url解码

          parseInt():将字符串转为数字
              * 逐一判断每一个字符是否是数字，直到不是数字为止，将前边数字部分转为number
          isNaN():判断一个值是否是NaN
              * NaN六亲不认，连自己都不认。NaN参与的==比较全部问false

          eval():讲 JavaScript 字符串，并把它作为脚本代码来执行。 
```


# 五、事件[重点]

js代码靠用户触发某些事件，触发js程序运行，当网页里的标签产生浏览器能够捕获的特定事件时，触发js代码运行。

### 1、事件模型的三要素

![](https://i.loli.net/2021/10/18/YFXLrcqNoUgmP3O.png)

```java
1.事件源：发生事件的事务/对象 通常为HTML页面的**标签** 对象
2.事件的属性：发生事件的性质(动作) 用户的某个操作（如：**单击、双击** 等）
3.监听器：事件发生后的处理 通常在**函数** 中定义处理程序
```


> 案例：点击按钮弹窗提示
事件源头：按钮标签
事件属性：单机
事件监听：弹窗提示


```html
<head>
    <script type="text/javascript">
    //函数 事件的监听功能
    function  test1(){
        alert("点击成功")
    }
    </script>
</head>
<body>
<!--准备事件源     绑定事件属性：监听用户的操作：单机事件-->
 <input type="button" value="单机" onclick="test1()">
</body>
```


事件示例图：

![](https://i.loli.net/2021/10/18/wDNhy5sJC8aInR6.png)

### 2、为html标签注册事件的方式

```java
<标签名 属性="" 事件名称="监听器">

如：
<input type="button" onclick="函数名字（参数列表）"/>
```


### 3、事件属性

```jaav
1.鼠标相关
**onclick 单击事件** ***** **
** **ondbclick 双击事件** ***
** **onmouseover 移入鼠标** ***
** **onmouseout 移出鼠标** ***** 
onmousemove 鼠标移动*
onmousedown 鼠标按下
onmouseup 鼠标松开

2.键盘相关
onKeyDown 按键按下*
onKeyUp 按键抬起*

3.body相关事件
onload //页面加载完毕后触发
onunload 窗口离开时触发(刷新，返回，前进，主页。。。。)
onscroll 页面滚动
onresize 缩放页面

4.form表单控件相关事件***
onblur 当前对象失去焦点
onchange 当前元素失去焦点，并且值改变时<input>(监听value属性值的修改)<select>
onfocus 当前对象获得焦点时 <input>
onsubmit 表单提交时（验证表单格式是否正确）
onreset  重置按钮被点击。

onsubmit例：
function xxx(){
if(...){
alert("错误消息");
return false;
}
return true;
}
<form...onsubmit="return xxx()">
```


### 4、注意事项

```java
1.事件冒泡：发生在内部元素的事件默认会向外部元素扩散
   取消事件冒泡===event.cancelBubble=true;
   event--代表监听的事件对象
   event.clientX--事件产生的横坐标
   event.clientY--事件产生的纵坐标
2.通过事件监听可以取消某些标签的默认行为[存在浏览器差异 IE不生效]
超链接标签--》<a href="跳转链接" onclick="return false">
表单标签--》<form action="" metho="get" onsubmit="return false">


```


