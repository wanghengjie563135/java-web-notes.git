# js中BOM实战项目之动态表格

# 一、动态删除/生成表格要求：

1. HTML标签只写三行表头

2. 通过JS来写动态的表格（有多少组数据，就自动创建多少行表格）

3. 通过JS来删除动态的表格（一行一行的表格）

4. 不涉及调用后台数据

先来看一下效果：

![](https://i.loli.net/2021/10/19/WzHMT3awgvLjx1S.png)

# 二、动态删除、添加的思路（知识点铺垫）:

#### （1）动态添加数据

> 1、创建文本节点：document.createTextNode(“文本”);
2、创建标签节点：document.createElement(“标签名”);
3、向父节点上追加节点： 父标签对象.appendChild(子标签对象)


```Java
1.创建文本节点：document.createTextNode(“文本”);
2.创建新的标签对象：
var tag=document.createElement("标签名");
eg:<h1>HelloWorld</h1>
    var tag=document.createElement("h1");
    tag.innerHTML="HelloWorld";
    
3.添加新标签对象：
parentTag.appendChild(newTag);-->将newTag添加到现有标签的最后面，称为父标签的最后一个孩子节点


```


![](https://i.loli.net/2021/10/19/5tYlJsucU9vGjF3.png)

- 案例：

```html
<head>
    <meta charset="UTF-8">
  <style>
    div{
      width: 100px;
      height: 100px;
      background-color: red;
    }
  </style>
  <script type="text/javascript">
    function  test1(){
      //1、创建div对象
        var div = document.createElement("div");
      //2.创建文本对象
        var textNode = document.createTextNode("whj");
      //3.将文本节点追加到div标签节点中,将textNode节点添加到div最后一个子节点上
        div.appendChild(textNode);
      //4.将div标签节点追加到当前页面中
        var body = document.getElementById("bd");
        body.appendChild(div);
    }
  </script>
</head>
<body id="bd">
<input type="button" value="单击创建div" onclick="test1()">
</body>
```


#### （2）动态删除

> 父标签对象.removeChild(子标签对象);


```jaav
1.删除标签对象：
parentTag.removeChild(childTag)-->从父标签对象中删除孩子标签对象

```


- 案例：单击删除div+div的whj文本

![](https://i.loli.net/2021/10/19/N49hMuBKYwjqfAp.png)

```html
<head>
    <meta charset="UTF-8">
    <style>
        div{
            width: 100px;
            height: 100px;
            background-color: red;
        }
    </style>
    <script type="text/javascript">
     function test1(){
         var body = document.getElementById("body");
         var div = document.getElementById("div");
         body.removeChild(div);
     }
    </script>
</head>

<body  id="body">
<div id="div">
    whj
</div>
<input type="submit" value="单击删除div+div的whj文本" onclick="test1()">
</body>
```


# 三、DOM实战-动态表格

![](https://i.loli.net/2021/10/19/JQmvhK1ABkE8IOi.png)

javascript实现动态删除、添加先捋一下思路：

要想实现动态效果，那就得使用增删节点的知识。

### 1、**动态表格添加数据分析：** 

> 事件源头：发送事件的标签 提交的标签
事件属性：用户的操作 单击
事件监听：将用户输入的数据填充到表格中
                   1、用户输入的数据
                    2、填充到表格里


```java
function addUser(){
  //1、通过标签元素value属性值 获取用户输入内容
  //2、将输入的内容封装成文本节点
  //3、创建td标签对象
  //4、将对应的文本节点放到td标签中
  //5、创建tr文本节点
  //6、将对应的td的标签放到tr标签中
  //7、将tr放到表格中
}
```


### 2、**动态表格删除数据分析：** 

> 事件源头：发送事件的标签 删除的按钮
事件属性：用户的操作 单击
事件监听：表格中的一行数据删除（tr）
                   1、获取删除哪一行数据
                    2、删除


```java
function removeUser(){
  //1、先获取事件源
      var buttonSource=event.target;
  //2.获取事件源的父标签（td）的父标签（tr）
      var tr=buttonSource.parentNode.parentNode;
  //3.获取tr标签的父标签（tbody或者是thead,为了避免找不到父标签使用parentNode）
      var parentNode=tr.parentNode;
  //4.删除这一行数据
     parentNode.removeClild(tr);
}
```


### 3、代码实现动态删除、添加表格：

#### （1）代码结构

![](https://i.loli.net/2021/10/19/sGV7952k1c8CQ6q.png)

#### （2）**HTML内容：** 

```html
<body>
<!--换行-->
</br>
<!--换行-->
</br>
<div id="div">
<!--居中-->
    <center>
        姓名：<input type="text" name="name" id="name">
        年龄：<input type="text" name="age" id="age">
        成绩：<input type="text" name="score" id="score">
        职位：<input type="text" name="job" id="job">
        <input type="button" id="button" value="添加" onclick="addUser()">
    </center>
</div>

<hr color="red"/>

<div class="container">

    <p class="btn btn-danger mb-3" data-table="example">DOM实战-动态表格</p>
    <table class="table table-bordered table-striped" id="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">姓名</th>
            <th scope="col">年龄</th>
            <th scope="col">成绩</th>
            <th scope="col">职位</th>
            <th scope="col">操作</th>
        </tr>
        </thead>
        <tbody id="tbody">
        <tr>
            <td>刘浩</td>
            <td>28</td>
            <td>100</td>
            <td>老师</td>
            <td>
                <input type="button" style="color: red" value="删除" onclick="deleteUser()">
            </td>
        </tr>
        <tr>
            <td>王恒杰</td>
            <td>21</td>
            <td>99</td>
            <td>组长</td>
            <td>
                <input type="button" style="color:red" value="删除" onclick="deleteUser()">
            </td>
        </tr>
        <tr>
            <td>张西</td>
            <td>20</td>
            <td>88</td>
            <td>学生</td>
            <td>
                <input type="button" style="color: red" value="删除" onclick="deleteUser()">
            </td>
        </tr>
        </tbody>
    </table>
</div>


</body>
```


####  （3）**CSS内容：** 

```HTML
   <meta charset="UTF-8">
    <title>动态表格</title>
    <link rel="stylesheet" href="http://cdn.bootstrapmb.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        .container {
            margin: 80px auto;
        }
    </style>
```


####    (4)  J**avaScript内容：** 

```html
   <script type="text/javascript">
            //将用户输入的数据填充到表格中
            function addUser(){
                //1、通过表单元素value属性值 获取用户输入内容  字符串
                var name = document.getElementById("name").value
                var tel = document.getElementById("tel").value
                var email = document.getElementById("email").value
                //2、将输入的内容封装成文本节点
                var nameNode = document.createTextNode(name);
                var telNode = document.createTextNode(tel);
                var emailNode = document.createTextNode(email);
                //3 、创建td标签对象
                var nameTd = document.createElement("td");
                var telTd = document.createElement("td");
                var emailTd = document.createElement("td");
                //4、将对应的文本节点放入对应的td里
                nameTd.appendChild(nameNode);
                telTd.appendChild(telNode);
                emailTd.appendChild(emailNode);
                //5、创建tr标签对象
                var tr = document.createElement("tr");
                //6、将td放入tr中
                tr.appendChild(nameTd)
                tr.appendChild(telTd)
                tr.appendChild(emailTd);
                //7、将tr放入表格中
                var table = document.getElementById("tb");
                table.appendChild(tr);
            }

            //动态删除  删除当前行数据
            function removeUser1(){
                //删除语法  父节点.removeChild(子节点)
                //通过事件对象获取事件发生的源头
                var delbtn = event.target ;
                //通过事件源获取父节点的父节点 就是tr标签对象
                var tr = delbtn.parentNode.parentNode;

                var parNode = tr.parentNode;

                parNode.removeChild(tr);
            }

            //动态删除  删除当前行数据
            function removeUser2(delbtn){
                //删除语法  父节点.removeChild(子节点)
                var tr = delbtn.parentNode.parentNode;

                var parNode = tr.parentNode;

                parNode.removeChild(tr);
            }
        </script>
```



#### 源代码在githee仓库：

**程序员小王Gitee:**  [https://gitee.com/wanghengjie563135/java-web-notes.git](https://gitee.com/wanghengjie563135/java-web-notes.git)


