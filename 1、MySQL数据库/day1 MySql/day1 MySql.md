# day1 MySql

# 数据库

- 存储数据的仓库，用于组织管理数据的软件

- 数据库是用来组织、存储、管理数据的系统。可以对其新增，删除，修改，查询的操作，并提供了保障数据完整性的事务机制以及容灾备份的能力

- 使用文件存储数据的劣势：

```java
1. 文件中存储数据，进行增删改查操作繁琐且低效 
2. 没有分配数据类型，文件中的数据都是字符串 
3. 缺乏对大数据集的优化，大文件操作速度会非常慢 
4. 缺乏共享，并发访问的支持，多个用户不能同时操作同1个文件 
5. 缺乏权限校验机制：身份认证、操作授权等等 
6. 没有备份容灾机制，不支持分布式缓存，机器一旦宕机存在极大的数据丢失风险
```


- 使用数据库存储数据的优势

```java
1. 可以持久化存储数据，具有特定的存储结构且支持分布式存储
2. 能够存储大量数据，自动压缩处理占用空间少 
3. 可以进行添加、修改、插入、删除等操作，而且方便、快捷 
4. 检索数据迅速、高效，支持索引技术，使得检索数据速度快，效率高 
5. 数据的共享性高、冗余度低且易扩充 
6. 安全性高，具备身份验证、操作授权等安全机制
7. 具备良好的容灾机制，自动备份、自动回滚，有效避免宕机带来的数据丢失风险
```


```markdown
从上世纪50年代末的理论研究开始，数据库历经了60多年的发展。数据库的数据模型从一开
始的层面模型，网状模型，关系模型，到对象模型，对象关系模型，半结构化等等。一些模
型已经是昨日黄花被时代抛弃，而有一些仍然处于理论研究阶段尚未落地，目前为止行业主
流数据模型仍是关系模型。
```


DBMS：数据库管理系统（Database Management System）是一种操纵和管理数据库的大型软件，例如建立、使用和维护数据库。

DB：数据库（Database）

SQL：结构化查询语言，（Structure Query Language），专门用来操作/访问数据库的通用语言。

## 一、数据库的分类

- 企业应用开发中使用到的数据库可以分为2种：传统的关系型数据库和新兴的NoSQL（Not OnlySQL）型数据库。

|特点|关系型数据库|NoSql数据库|
|---|---|---|
|存储的数据结构|结构化的数据，以表（table）为单位存储数据，由行(row)列（colum)组成表，由多张表组成库，数据与数据之间可以存在关系|半结构化的数据（xml,json)                             非结构化的数据（文档、文本，图片、视频、音频）<br />数据独立相互没有任何关系，以Key-Value的形式存储|
|事务（保证数据完整性的手段）|强事务,保障数据的安全|弱事务性，有些直接不支持事务|
|性能|高并发情况下性能较差，安全性高，不丢失数据，用来存储重要的数据|高并发下性能较好，安全性低，存储视频|



例：关系型数据库中的数据

![](https://i.loli.net/2021/09/28/dynjAbGQO7z4XVU.png)

![](image/image_1.png)

例：NoSql数据库中的数据

![](https://i.loli.net/2021/09/28/iafyup6k1oTrc7S.png)

- 实战开发时，关系型数据库和NoSQL数据库相辅相成

```Markdown
1. 关系型数据库：对事务要求比较高的数据，例如金融类数据 
2. NoSQL型数据库：对事务要求低，对性能要求高的数据，例如 聊天记录、商品详情
```


- 常见的数据库产品

```java
1. 关系型数据库
   Oracle（付费）
   DB2（IMB数据库、付费）
   SQLServer（主流、微软、付费） 
   MySQL（免费、开源）（被oracle收购）
   SQLite（免费、嵌入式、轻量级、Android自带）
   H2（免费、Java实现、嵌入式、轻量级）
2. NoSQL型数据库 
   Redis、MongoDB、HBase 以上三款全部开源免费
```


行业背景：

```java
互联网技术在近 10 多年发生了翻天覆地的变化，最早是以 IOE 架构为代表，即：IBM 的服
务器、Oracle 数据库、EMC 存储设备。IOE 几乎是全世界大公司的“黄金搭档”。国内的银
行、电信、证券这些不差钱的行业很喜欢 IOE。这些传统大企业的业务规模变化稳定，IOE
足够支撑日常业务需求，公司本身又有足够的 IT 预算。
```


```java
直到互联网公司崛起，互联网公司的业务增长已经不再是传统公司的线性模式，而是指数级
爆发式的增长。为了应对这种业务增长速度，服务海量的用户，最早像谷歌、Amazon、
Facebook、阿里等等国内外的互联网巨头纷纷选择用 X86 通用服务器、开源数据库
MySQL，以及分布式存储代替 IOE 架构，经过 10 多年的发展，这股风潮已经逐渐从互联网
企业蔓延到传统企业。
```


我们的课程中首先学习**关系型数据库MySQL** ，而后续课程中**学习NoSQL型的Redis** 

## 二、MySQL数据库

#### 1. 开放源代码

MySQL最强大的优势之一在于它是一个开放源代码的数据库管理系统。开源的特点是给予了用户根据自己需要修改DBMS的自由。MySQL采用了General Public License，这意味着授予用户阅读、修改和优化源代码的权利，这样即使是免费版的MySQL的功能也足够强大，这也是为什么MySQL越来越受欢迎的主要原因。

#### 2. 跨平台

MySQL可以在不同的操作系统下运行，简单地说，MySQL可以支持Windows系统、UNIX系统、Linux系统等多种操作系统平台。这意味着在一个操作系统中实现的应用程序可以很方便地移植到其他的操作系统下。

#### 3. 轻量级

MySQL的核心程序完全采用多线程编程，这些线程都是轻量级的进程，它在灵活地为用户提供服务的同时，又不会占用过多的系统资源。因此MySQL能够更快速、高效的处理数据。

#### 4. 成本低

MySQL分为社区版和企业版，社区版是完全免费的，而企业版是收费的。即使在开发中需要用到一些付费的附加功能，价格相对于昂贵的Oracle、DB2等也是有很大优势的。其实免费的社区版也支持多种数据类型和正规的SQL查询语言，能够对数据进行各种查询、增加、删除、修改等操作，所以一般情况下社区版就可以满足开发需求了，而对数据库可靠性要求比较高的企业可以选择企业版。

![](https://i.loli.net/2021/09/28/Z7umLDPS1bl8HtC.png)

```java
在WEB应用方面MySQL是最好的RDBMS(Relational Database Management System：
       关系数据库管理系统)应用软件之一
。由瑞典MySQL AB公司开发，目前属于Oracle公司。

 1. 开源免费，不需要支付额外的费用，可以修改源码进行定制。 
 2. 可以处理单表拥有上千万条记录的大型数据库。
 3. 支持标准的SQL语句
 4. 可以应用于多个系统上，并且支持多种语言。(C、C++、Python、Java等)
```


```java
FAT32 单个文件 4G
NTFS 单个文件 2T
GB 吉字节、TB太字节、PB拍字节、EB艾字节、ZB泽字节、YB尧字节
```


- cmd启动mysql

- 命令行（使用命令行的前提需要配置mysql的环境变量）

```sql
mysql -u root -p
```


## 三、MySQL数据库的安装和使用

### 1、数据库的工作模型

- C/S结构工作模型：Client(客户端）+Server(服务器）

       可以被多个客户端共享使用，不限制客户端是否在本地

&ensp;&ensp;&ensp;&ensp;    客户端：访问数据库的软件

```markdwon
mysql：存储数据，管理数据的数据库
navicat:主流的专业的Sql客户端，支持中文
```


- 单机工作模型

软件同一时刻只能为一个用户提供服务，如果有多个用户需要使用，必须在使用软件的每一台电脑上安装软件。典型软件有office、输入法、视频播放器...

![](https://i.loli.net/2021/09/28/j3QVtJY2vC8FzfN.png)

- 客户端-服务器工作模型

&ensp;&ensp;&ensp;&ensp;软件同一时刻可同时为多个用户提供服务，即使有多个用户需要使用也不需要多次安装。只需要在一台电脑上安装软件后即可对外提供服务（所以称为服务端），用户只需要在各自的电脑上安装用于连接服务器的客户端软件即可。

&ensp;&ensp;&ensp;&ensp;SVN server 与 SVN viver

![](https://i.loli.net/2021/09/28/VKUzSBsoh85QN1a.png)

### 2、 数据库（MySQL）和客户端（Navicat）的安装

- 参考数据库安装文档

### 3、Navicat客户端的使用

[employees.sql](file/employees.sql)

1. 建立连接

![](https://i.loli.net/2021/09/28/hHOSC9gpYJuWMni.png)

配置链接参数并测试链接

![](https://i.loli.net/2021/09/28/ytUDw4EfbXFc3sm.png)

2. 打开用户链接

![](https://i.loli.net/2021/09/28/7FwMLWPimxDgfj4.png)

3. MySQL内置数据库简介

```java
1. information_schema：存储数据库信息或表的名称，列的数据类型或访问权限等 
2. mysql：核心数据库，主要负责存储数据库的用户、权限设置、关键字等mysql自己需要使用的控制和管理信息 
3. perfrmace_schema：主要用于收集存放数据库的性能参数
4. sys：包含了一系列方便 DBA 和开发人员利用 performance_schema 性能数据库进行性能调优 和诊断的视图。
```


4. 新建数据库，用于练习、测试

![](https://i.loli.net/2021/09/28/odrxJ2iHgaLTsQS.png)

配置数据库相关参数

![](https://i.loli.net/2021/09/28/uJ6D1cxIsPwgRrO.png)

务必保证字符集与排序规则与上图一致，否则可能会出现乱码问题！

5. 导入数据便于学习与测试

![](https://i.loli.net/2021/09/28/Eu6Kh85qkCl9nxV.png)

导入成功显示：

![](https://i.loli.net/2021/09/28/IBSZLp57cCOhGzJ.png)

双击库表，查看表中数据

![](https://i.loli.net/2021/09/28/LoT81kIrQy7dexv.png)

![](https://i.loli.net/2021/09/28/2is9OyQhrTRzeoY.png)

## 四、MySQL库表相关概念

#### 1、库表主体结构

![](https://i.loli.net/2021/09/28/245PyRODiQwSF7I.png)

1. 表 （table）

表是数据库组织、存储数据的基本单位，表中保存着多行多列的数据。不同类别的数据保存到不同

的表中，比如:用户信息保存在user表中，学员信息保存在student表中，员工信息保存在

employees表中。

2. 行（row）

行是表中存储数据的基本单位，一行数据通常包含多个字段（列）。向表中添加数据，保证一行

数据的完整性。

3. 字段（列 column）

一个字段用来描述一行数据的特征。比如：用户要包含用户名、密码等内容，user表中就有用户

名、密码这些字段（列）。

4. 主键（PrimaryKey）

本质上就是特殊的字段：用以在表中充当唯一的标识，是一行数据最重要的字段。主键要求：唯

一、不能为null

5. 外键（ForeignKey）

也是一个特殊的字段：用来记录表和表之间关系的列，比如：员工一定有其所属的部门，那么

employees 员工表和 departments 部门表就存在关系，为了记录员工的部门，就可以在员工表中

定义一列 department_id 保存自己所属部门的id（对应着departments中的 department_id 主

键），这样员工表 department_id 就记录了员工和部门的关系。 employees 表的 employee_id

就是一个外键。

![](https://i.loli.net/2021/09/28/M3d82HToAlxBr4a.png)

1. 数据库

为了方便对表管理，MySQL中通过库管理表，通常一个项目对应一个库。

- SQL（Structured Query Language 结构化查询语言） 是用于访问和处理数据库的标准的计算机语言，核心功能是提供了对表中数据增、删、改、查操作。

```java
需要注意的是：SQL是一种国际标准，并不为哪个数据库独有，最普及的是SQL92标准。所
有的关系型数据库都支持SQL，并在标准语法外提供了各自的私有扩展（不用过于担心私有
扩展带来的碎片化和学习成本的增加，私有扩展只是很少的一部分）。绝大多数的SQL语句
可以不用修改的在任何数据库正确运行。
```


- 使用Navicat执行SQL

![](https://i.loli.net/2021/09/28/SVCZQfKsYpJy7nI.png)

- 跟大多数语言一样，SQL语句也需要使用";"号表示一行语句的结束

#### 2、MySQL的语法规范和要求

（1）mysql的sql语法不区分大小写

> MySQL的关键字和函数名等不区分大小写，但是对于数据值是否区分大小写，和字符集与校对规则有关。


**_ci（大小写不敏感），_cs（大小写敏感）** ，_bin（二元，即比较是基于字符编码的值而与language无关，区分大小写）

（2）命名时：尽量使用26个英文字母大小写，数字0-9，下划线，不要使用其他符号

（3）建议不要使用mysql的关键字等来作为表名、字段名等，如果不小心使用，请在SQL语句中使用`（飘号）引起来

（4）数据库和表名、字段名等对象名中间不要包含空格

（5）同一个mysql软件中，数据库不能同名，同一个库中，表不能重名，同一个表中，字段不能重名

（6）标点符号：

> 必须成对
必须英文状态下半角输入方式
字符串和日期类型可以使用单引号''
列的别名可以使用双引号""，给表名取别名不要使用双引号。取别名时as可以省略
如果列的别名没有包含空格，可以省略双引号，如果有空格双引号不能省略。


（7）SQL脚本中如何加注释

> 单行注释：#注释内容

单行注释：--空格注释内容    其中--后面的空格必须有

多行注释：/* 注释内容 */


```mysql
#以下两句是一样的，不区分大小写
show databases;
SHOW DATABASES;

#创建表格
#create table student info(...); #表名错误，因为表名有空格
create table student_info(...); 

#其中name使用``飘号，因为name和系统关键字或系统函数名等预定义标识符重名了。
CREATE TABLE t_stu(
    id INT,
    `name` VARCHAR(20)
);

select id as "编号", `name` as "姓名" from t_stu; #起别名时，as都可以省略
select id as 编号, `name` as 姓名 from t_stu; #如果字段别名中没有空格，那么可以省略""
select id as 编号, `name` as 姓 名 from t_stu; #错误，如果字段别名中有空格，那么不能省略""
```


#### 3、 SQL分类

DDL (Data Definition Language) ：数据定义语言，定义库，表结构等，包括create,drop,alter等

DML (Data Manipulation Language) ：数据操作语言，增删改查数据，包括insert,delete,update,select等

DQL(Data query Language):数据查询语言 select

DCL（Data Control Language） ：数据控制语言，权限，事务等管理。

## 六、简单查询

#### 1、查询所有列

- 语法

```java
select * from 表名; -- 第一种 

select 列名1,列名2,...，最后一个列名 -- 第二种 
from 表名;
```


例:

```Java
select * from employees; 
select first_name, last_name, email , 
phone_number, job_id ,salary , 
commission_pct,
manager_id,department_id ,
hiredate  
from employees;
```


实际开发中使用第二种，可读性好

#### 2、查询部分列

- 语法：

```java
select 列名1,列名2,... from 表名;
```


例：

```java
select employee_id,
first_name,email,
salary from employees;
```


#### 3、查询结果运算

- 语法：

```java
select 数字列名 运算符 数据 
from 表名;
```


- 例：查询所有员工姓名与年薪

```java
select first_name , salary * 12 from employees;
```


例：查询所有员工姓名与薪资+编号的结果

```java
select first_name , 
salary + employee_id from employees;
```


数据可以来源于一个数值，也可以是另外一列

#### 4、修改查询结果中的字段名

- 语法：

```SQL
select 列名 as 别名,列名2 as 别名2 from 表名;
```


例：显示所有员工的月薪与年薪

```SQL
select salary as 月薪, salary * 12 as 年薪 from employees;
```


可以省略as关键字，例如：

```SQL
select salary 月薪, salary * 12 年薪 from employees;
```


#### 5、去除重复数据

- 语法：

```SQL
select distinct 列名1,列名2,..... from 表名; 
-- 当列名为多个时，所有列的值都重复时才认为重复
```


例：

```SQL
select **distinct**  job_id from employees;
```


#### 6、条件分支

- 语法：

```SQL
case
 when 条件1 then 结果1 
 when 条件2 then 结果2 
 when 条件3 then 结果3 
 ... 
 else 其他结果 
 end 
 -- 满足条件返回对应的结果，当所有条件都不满足时，返回else中的结果
```


例：

```SQL
select first_name,salary,
      case
      when salary>10000 then '优秀'
      when salary>8000 then  '中等'
      when salary>5000 then '一般'
      else '重修学习'
      end as 薪资等级
      from employees;
```


#### 7、查询表结构

- 语法：

```SQL
desc 表名;
 -- describe
```


例：

```SQL
desc emplyees;
```


## 七、条件查询

#### 1、单条件查询

- 支持关系运算符：> < >= <= = !=

- 语法：

```SQL
select 列名,... 
from 表名 
where 条件;
```


例：查询薪资大于等于8000的员工信息

```SQL
select * from employees where salary >= 8000;
```


例：查询员工编号大于130的员工信息

```SQL
select * from employees where emplyee_id > 130;
```


例：查询first_name为steven的员工信息

```SQL
select * 
from employees 
where first_name = 'steven'; 
-- 注意：mysql针对字符串比较时默认不区分大小写，如要区分大小写添加关键字binary
```


```java
select * 
from employees 
where binary first_name = 'steven';
```


#### 2、多条件查询

- 语法：

```SQL
select 列名,... 
from 表名 
where 条件1 链接条件 条件2...; 
and 连接条件，连接的所有条件必须同时成立（&&） 
or 连接条件，连接的条件任意一个成立即可以（||）
```


例：查询薪资大于2500且低于8000的员工信息：

```SQL
示例：查询薪资大于等于8000且薪资小于等于15000的员工
 select * 
 from employees where salary >= 2500 and salary <= 8000;
```


例：查询薪资大于等于10000或小于等于3500的员工

```SQL
select * 
from employees 
where salary > 10000 or salary <= 3500;
```


#### 3、区间查询

语法：

```SQL
select 列名,... 
from 表名 
where 列名 [not] between 起始值 and 结束值;
```


例：查询薪资在8000~15000范围的数据

```SQL
select * 
from employees 
where salary between 8000 and 15000;
```


例：查询薪资不在8000~15000之间的数据

```SQL
select *
 from employees 
 where salary not between 8000 and 15000;
```


#### 4、枚举查询

- 语法：

```SQL
select 列名,... 
from 表名 
where 列名 [not] in(值1,值2,值3...);
```


例：查询department_id为60/90/100的员工数据

```SQL
select * 
from employees 
where department_id in (60,90,100);
```


例：查询department_id不是60/90/100的员工数据

```sql
 select *
 from employees 
 where department_id not in (60,90,100);
```


#### 5、查询空值

- 语法：

```SQL
select * 
from 表名 
where 列名 is [not] null;
```


例：查询没有提成的员工

```SQL
select * 
from employees 
where commission_pct is null;
```


例：查询有提成的员工

```SQL
select * 
from employees 
where commission_pct is not null;
```


#### 6、模糊查询

- 语法：

```SQL
select * 
from 表名 
where 列名 like '通配模式'; 
% 通配模式,表示n个任意的字符 
_ 通配模式,表示1个任意的字符
```


例：查询first_name 以A开头的员工

- 错误语法

```·java
There is an error in your SQL syntax; 
Check the manual corresponding to
 your MySQL server version for the 
 correct syntax used near line 3, "A%"
您的SQL语法有错误；查看与您的MySQL服务器版本相对应的手册，
了解在第3行“A%”附近使用的正确语法
```


```SQL
select * 
from employees 
where first_name like 'A%';
```


例：查询first_name 包含a的员工

```SQL
select *
from employees
where first_name like '%a%';
```


例：查询first_name第3个字符为a的员工

```SQL
select * 

from employees 

where first_name like '__a%';

 -- 注意：示例中使用的是2个下划线
```


## 八、排序

#### 1、单列排序

- 语法：

```SQL
select 列名 
from 表名 
where 条件 
order by 列名 排序规则 

排序规则：asc(默认值，升序) desc(降序);
```


注意：order by 语句只能出现在最后一行

- 例：查看员工所有信息，根据薪资升序排列查询结果

```SQL
select * 
from employees 
order by salary asc
```


例：查看员工所有信息，根据薪资降序排列查询结果

```SQL
select * 
from employees 
order by salary desc;
```


#### 2、多列排序

- 语法：

```SQL
select 列名,... 
from 表名 
where 条件 
order by 列名1 排序规则，列名2 排序规则,... 

排序效果：
整体先根据列1进行排序，
如果列1值相同时，再根据列2值排序，以此类推。
```


例：先根据薪资降序排列，然后薪资相同根据id升序排列

```SQL
select * 
from employees 
order by salary desc,employee_id asc;
```


例：条件查询和排序联合使用，找出所有工资大于8000的员工，升序排列结果

```SQL
select * 
from employees 
where salary >= 8000 
order by salary desc;
```


## 九、单行函数


- 概念：作用在一行数据，一行数据会对应一个结果

#### 1、concat【合并】

- 作用：合并多列为一列

- 语法：

```SQL
select concat(列名1,列名2...)
 from 表名;
```


例：将员工表中的 first_name 与 last_name 拼接为一列

```SQL
select concat(first_name,last_name) 姓名 
from employees;
```


例：使用字符或数字进行拼接

```SQL
select concat(first_name,'-',last_name) 姓名 
from employees;
```


#### 2、 mod 【可选】

- 作用：mod( m, n )等同于数学运算中的%，标准的SQL中并不支持直接使用%，使用mod()更有通用性。

- 语法：

```SQL
select mod(列名1,列名2) 
from dual; 

-- dual（虚表，只为满足语法使用，主要查看函数的执行效果）
-- 列名可替换为数字
--dual:双数
```


例：

```SQL
select mod(5,2) 
from dual;
 相当于Java中的5%2
```


#### 3、length

- 作用：length( 列名 )，可以获取该列每行数据的长度

- 语法：

```SQL
select length( 列名 ) 
from 表名;
```


例：查询员工表中 first_name 每一行数据的长度

```SQL
select length(first_name) 

from employees;
```


例：查询员工表中 first_name 字符个数小于5的员工

```SQL
select * 
from employees 
where length(first_name) < 5 ;
```


#### 4、sysdate

- 作用：sysdate( )获取当前系统时间

- 语法：

```SQL
select sysdate() 
[from 表名];
```


例：获取当前系统时间

```sql
select sysdate() 

from dual; 

-- from dual 可以省略 

select sysdate()
```


#### 5、date_format

- 语法：date_format(date,format) 将日期转换固定格式的字符串

- format日期格式：

|格式标识符|含义|
|---|---|
|%Y|年（4位）|
|%y|年（2位）|
|%m|月，格式为（01~12)|
|%c|月，格式为（1~12）|
|%d|天，格式为（00~31）|
|%e|天，格式为（0~31）|
|%H|小时，格式为（00~23）|
|%k|小时，格式为（0~23）|
|%i|分钟，格式为（00~59）|
|%s|秒，格式为（00~59）|



注意：日期格式为字符串，需要使用定义在单引号（' ') 中

- 例：将employees表中的 hiredata 转换为xxxx年xx月xx日 时:分:秒 的形式

```SQL
select date_format(hiredate,'%Y年%m月%d日 %H:%i:%s')
 
from employees
```


#### 6、 str_to_date

- 作用：str_to_date( str , format ) 将日期格式的字符串转换为日期数据。

- 例：将'2020-01-06 10:20:30' 字符串转换为相应的日期数据

```SQL
select str_to_date('2020-01-06 10:20:30','%Y-%m-%d %H:%i:%s')
```


#### 7、date_format

- 语法：date_format(date,format)：将日期和时间数据转换为指定的字符串。

```sql
select date_format(now(),'%Y-%m-%d %H:%i:%s');
```


## 十、组函数

- 概念：作用到一组（多行）数据，每组数据得到一个结果，一张表没有显式分组前，默认当做一个组处理。

- 语法：

```SQL
select 组行数1(列名) , 组函数2(列名)...from 表名;
```


- 组函数

|组函数|作用|
|---|---|
|max(列）|获取最大值|
|min(列)|获取最小值|
|sum(列)|获取总和|
|avg(列)|获取平均值|
|count(列)|统计个数|



注意：所有组函数统计时自动忽略null值。

例：查看员工的最高工资

```SQL
select max(salary) 
from employees;
```


例：查看员工的平均工资与员工数量

```sql
select avg(salary) , count(employee_id) 
from employees;
```


例：统计部门个数

```sql
step1：找出所有部门（去重）
 select distinct department_id 
 from employees; 
 
 step2：统计数量 
 
 select count(distinct department_id)
 
  from employees;
```


![](image/image_18.png)

命令行（使用命令行的前提需要配置mysql的环境变量）

