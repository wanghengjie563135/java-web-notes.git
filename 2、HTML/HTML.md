# HTML

# HTML前提：



#### 单词

1. align:排列，定义位置

2. target:目标，值使用_blank,通过新的网页打开href的网址


#### 单词

1. comment:评论

# 一、Hbuilder的格式化插件说明

[https://ask.dcloud.net.cn/article/36529](https://ask.dcloud.net.cn/article/36529)

 `1. HBuilderX的格式化插件说明`

|插件名称|对应插件配置中的名称|是否内置|可格式化的文件|插件市场|
|---|---|---|---|---|
|js-beautify|format|内置插件|vue、html、js、css、json| |
|prettier|format-prettier|非内置，需要下载|less、sass、vue、stylus(vue内嵌)、ts、yaml|[下载地址](https://ext.dcloud.net.cn/plugin?id=2025)|
|stylus-supremacy|format-stylus-supremacy|非内置，需要下载|格式化单独stylus文件|[下载地址](https://ext.dcloud.net.cn/plugin?id=2039)|



# 二、Internet - 国际互联网

#### 1. 概念： 

Internet也称 因特网 、国际互联网、网际网 等。由若干个终端以及传输介质构成。

#### 2. 基于Internet所提供的服务：

&ensp;&ensp;&ensp;&ensp;2.1 信息共享

&ensp;&ensp;&ensp;&ensp;2.2 文件的上传和下载

&ensp;&ensp;&ensp;&ensp;2.3 邮件  Email

#### 3. IP地址  计算机的唯一标识  

组成：0-225 0-225 0-225 0-225

```java
公网ip：国际互联网的唯一标识
局域网ip:局部区域内的唯一标识
```


#### 4.  域名

计算机ip地址的字符串表现形式

```java
电话： 15120308630  电话本：王恒杰  通过王恒杰就可以直接访问
百度ip地址：119.75.217.109   域名：https://www.baidu.com/

```


#### 5.DNS域名解析服务器

```java
dns:域名解析服务器 ：
计算机记录ip地址  
119.75.217.109=https://www.baidu.com/
```


- 域名分层

```java
域名是分级的，一般分为：主机名.三级域名.二级域名.顶级域名.
注意，最后一个点代表的是根域，是所有域名的起点。
域名有点像美国人的姓名一样，姓在后，名在前，而计算机域名中最后的点则是根，
其次是根下的顶级域名，然后是二级域名等。
图1为典型的域名树状结构图。

注意，一般情况下，我们通过浏览器输入网址域名时，
最后一个根域（.）是不需要输入的。一般顶级域代表国家或者组织形式，
如cn代表中国；
com代表商业公司；
edu代表教育机构等。
二级域名代表组织或者公司名称，
三级域名代表组织或者公司内部的主机名称。
最后通过完全合格的域名（FQDN）可以定位全球唯一的一台主机。
这种分层管理机制的优势在于根域服务器不需要管理全世界所有的域名信息，
它只需要管理顶级域信息即可，而顶级域服务器只需要管理二级域信息即可。
以此类推，实现分层管理，这类似国家的行政管理机制。
```


```java
例如：百度的域名为www.baidu.com,
代表的是根域下有com子域，
com子域下面有baidu子域，
baidu子域下有主机www。
```


![](https://i.loli.net/2021/10/07/AxfuVLGqFe4b9I1.png)

#### 6. 端口 port 

```java
端口：软件在计算机中的唯一标识
 127.0.01:3306【端口号】 
 端口号范围：1~65535
 
 一般是指TCP/IP协议中的端口，端口号的范围从0到65535，
 比如用于浏览网页服务的80端口，用于FTP服务的21端口

注意：软件Tomcat Oracle 用的端口都是8080，
     会出现端口冲突
     
    改端口号：1024以下的端口 系统默认占用，避开1024以下
    3306：Mysql
    8080:Oracle Tomact 
```


#### 7. 网络协议

```java
指定网络中数据传输的形式
http:web应用
超文本传输协议（Hyper Text Transfer Protocol，HTTP）
是一个简单的请求-响应协议，它通常运行在TCP之上。
它指定了客户端可能发送给服务器什么样的消息以及得到什么样的响应。
请求和响应消息的头以ASCII形式给出；而消息内容则具有一个类似MIME的格式。

端口：80
服务：HTTP
说明：用于网页浏览。木马Executor开放此端口。

ftp:
文件传输协议（File Transfer Protocol，FTP）
是用于在网络上进行文件传输的一套标准协议，它工作在 OSI 模型的第七层，
TCP 模型的第四层， 即应用层， 使用 TCP 传输而不是 UDP， 
客户在和服务器建立连接前要经过一个“三次握手”的过程，
保证客户与服务器之间的连接是可靠的， 
而且是面向连接， 为数据传输提供可靠保证。

端口：21
服务：FTP
说明：FTP服务器所开放的端口，用于上传、下载。
最常见的攻击者用于寻找打开anonymous的FTP服务器的方法。 

```


```Java
协议的组成：协议头 协议体
协议头（head):数据的数据 元数据 明文传递，不安全，数据量有限（4kb)
协议体（body):需要传输的数据  密文传递，安全，没有数据量限制
```


编码的表现：电台 

密码本：计算机网络数据传输有一个密码本，就是网络协议

![](https://i.loli.net/2021/10/07/du7lhqLBimotgKn.png)

#### 8. 服务器

服务器就是一台计算机，计算机的主要功能为其他计算机提供服务，所以叫做服务器

```java
餐厅 服务员：请求服务员为你提供服务    如：我要一张纸，他把纸给你之后就是一个请求
计算机：服务器  请求服务器发送一个请求  如：使用百度的首页，响应以后，把页面给你

计算机：接收请求 处理请求 返回响应  **把提供服务的这类型的计算机叫做服务器** 
```


宕机：计算机死机了

服务器集群：很多服务器

```java
服务器集群就是指将很多服务器集中起来一起进行同一种服务，
在客户端看来就像是只有一个服务器。
集群可以利用多个计算机进行并行计算从而获得很高的计算速度，
也可以用多个计算机做备份，从而使得任何一个机器坏了整个系统还是能正常运行。
```


![](https://i.loli.net/2021/10/07/i7CPpr1sLD6HyEG.png)

- 服务器分类：

```java
pc: 柜式，机架式，刀片式  **学校** 
小型机服务器：80万~100万  Oracle IBM 生产 维护成本高
```


- 网站

```java
1.计算机
2.公网ip  119.75.217.109
3.域名 注册 【阿里云】
```


[服务器能做什么.xmind](file/%E6%9C%8D%E5%8A%A1%E5%99%A8%E8%83%BD%E5%81%9A%E4%BB%80%E4%B9%88.xmind)

#### 9、网络传输的三大基石

```java
三大基石：**URL，HTTP协议，HTML** 

URL：在WWW上，每一信息资源都有统一的且在网上唯一的地址，
  该地址就叫URL（Uniform Resource Locator,统一资源定位符），
  它是WWW的统一资源定位标志，就是指网络地址。

HTTP协议：http是一个简单的请求-响应协议，它通常运行在TCP之上。
    它指定了客户端可能发送给服务器什么样的消息以及得到什么样的响应。
    请求和响应消息的头以ASCII码形式给出；而消息内容则具有一个类似MIME的格式。
    这个简单模型是早期Web成功的有功之臣，因为它使得开发和部署是那么的直截了当。

HTML：HTML称为超文本标记语言。


```


![](https://i.loli.net/2021/10/07/TO2SiNrDK8GmguU.png)

# 三、Web 的概念

1. 什么是WEB ： 基于Internet提供的一种图形化的信息服务 。核心基于超文本(文本、音频、图片、视频等信息)和HTTP(协议)的服务。

2. WEB应用： 通常就是指一个网站系统。

3. WEB应用的基本工作原理：

![](https://i.loli.net/2021/10/07/yiHuF5rv8MmeNfL.png)

4. WEB中的相关技术：

4.1 HTML 、CSS 、javascript 、JSP 、ASP 、PHP


三、HTML概述


&ensp;&ensp;&ensp;&ensp;1. 什么是HTML
&ensp;&ensp;&ensp;&ensp;

```java
1.1 HTML- Hyper Text Markup Language 全称超文本标记语言 。
*文本就是字符，超文本：**除了文本文档除了存储字符之外，图片 视频 音频 链接等** 
*标记语言：用标记来展示数据 标记：标签 <标签>内容</标签>
*本质：互联网中的静态网页，展示内容
1.2 HTML不属于编程语言，没有一些特殊的逻辑性的含义。用来展示数据
1.3 HTML文档中包含了 HTML标签 和 文本内容。
1.4 HTML文档的格式 是以 .html 或 .htm 结尾的文件
```


&ensp;&ensp;&ensp;&ensp;2. HTML主要作用： 用于编写网页， 给网页中的内容增加风格和样式。

&ensp;&ensp;&ensp;&ensp;- 运行方式

![](https://i.loli.net/2021/10/07/RCKbPXm5J86nIvi.png)

&ensp;&ensp;&ensp;&ensp;3. HTML的编辑：

```Java
3.1 书写
几个常用的文本编辑器： notepad++ 、Sublime Text 、 HBuilder
3.2 运行
HTML文档无法独立运行， 必须基于浏览器来运行。
主流的浏览器： IE、 Google 、火狐 、 Edge
3.3 注释： <!-- 内容 -->
```


- 架构：静态页面放在WEB下，和WEB-INF平级

![](https://i.loli.net/2021/10/07/PpQA71nZkHu4eiB.png)

4. HTML标签的基本语法：

```java
4.1 标签是由两个尖角号包含： <标签名>

4.2 HTML 标签一般是成对出现的 ： 比如 <p> 内容 </p>

<开始标签> ... </结束标签> 注意： 结束标签必须有 斜杠

4.3 有一些特殊的标签，是单独出现的 , 称为单标签 ：  比如 <br/> <hr/>

4.4 标签可以有属性：

<标签名 属性名=”属性值” 属性名=”属性值”> .. </标签名>

注意： 属性定义在开始标签中
```


# 四、HTML核心内容

## 1、语法

- 标签:利用标记的形式,通过特殊符号定义网页

```html
标签通常成对存在:
<标签名>标签体</标签名> 
存在单标签:<标签名/>
 <title>this is my first html</title>
 <hr/> 

补:注释<!--注释内容-->使用快捷键 Ctrl+/ 注释一行内容
```


- 属性:进一步说明标签

```html
属性名="属性值" color="red"
 <标签名 属性名="属性值"></标签名> 
 例:<hr color="red"></hr>
  <标签名 属性名="属性值"/>
   例:<hr color="red"/> 
   属性可以添加多个,中间使用空格隔开 
   例:<hr color="red" size="7"></hr>
```



## 2、页面骨架

![](https://i.loli.net/2021/10/07/P6rCR1iDFnM3xoe.png)

```html
<!DOCTYPE html>
<!--根标签-->
<html lang="en">
<!--头标签：对网页的设置内容 存放位置-->
<head>
<!--    编码，作者，创建时间，关键字 css样式 js-->
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<!--体标签-->
<!--能看到的内容，用于存放展示的数据内容-->
<body>

</body>
</html>

【1】html标签
定义 HTML 文档，这个元素我们浏览器看到后就明白这是个HTML文档了，
所以你的其它元素要包裹在它里面，标签限定了文档的开始点和结束点，在它们之间是文档的头部和主体。

【2】head标签---》里面放的是页面的配置信息
head标签用于定义文档的头部，它是所有头部元素的容器。
<head> 中的元素可以引用脚本、指示浏览器在哪里找到样式表。
文档的头部描述了文档的各种属性和信息，包括文档的标题、在 Web 中的位置以及和其他文档的关系等。
绝大多数文档头部包含的数据都不会真正作为内容显示给读者。
下面这些标签可用在 head 部分：
<title>、<meta>、<link>、<style>、 <script>、 <base>。
应该把 <head> 标签放在文档的开始处，紧跟在 <html> 后面，并处于 <body> 标签之前。
文档的头部经常会包含一些 <meta> 标签，用来告诉浏览器关于文档的附加信息。

【3】body标签---》里面放的就是页面上展示出来的内容
body 元素是定义文档的主体。
body 元素包含文档的所有内容（比如文本、超链接、图像、表格和列表等等。）
body是用在网页中的一种HTML标签，标签是用在网页中的一种HTML标签，表示网页的主体部分，
也就是用户可以看到的内容，可以包含文本、图片、音频、视频等各种内容！

```


1. <head>可以省略

2. **Html标签不区分大小写** 

3. html大部分都是成对出现的<标签名></标签> 

4. 特殊情况 <br/> <hr/> <meta/>单体标签

5. 标签内部可以书写属性

6. 标签之间可以嵌套使用

#### head中可以用的标签

```html
<html>
        <!-- 这是一个注释，注释的快捷键是ctrl+shift+/-->
        <!--
                head标签中：放入：页面的配置信息
                head标签中可以加入：
                <title>、<meta>、<link>、<style>、 <script>、 <base>。
        -->
        <head>
                <!--页面标题-->
                <title>百度一下，你就知道</title>
                <!--设置页面的编码，防止乱码现象
                        利用meta标签，
                        charset="utf-8" 这是属性，以键值对的形式给出  k=v a=b 
                        告诉浏览器用utf-8来解析这个html文档
                -->
                <meta charset="utf-8" /><!--简写-->
                <!--繁写形式：（了解）-->
                <!--<meta http-equiv="content-type" content="text/html;charset=utf-8" />-->
                <!--页面刷新效果-->
                <!--<meta http-equiv="refresh" content="3;https://www.baidu.com" />-->
                <!--页面作者-->
                <meta name="author" content="msb;213412@qq.com" />
                <!--设置页面搜索的关键字-->
                <meta name="keywords" content="线上培训;架构师课程" />
                <!--页面描述-->
                <meta name="description" content="详情页" />
                <!--link标签引入外部资源-->
                <link rel="shortcut icon" href="https://www.baidu.com/favicon.ico" type="image/x-icon" />
        </head>
        <!--
                body标签中：放入：页面展示的内容
        -->
        <body>
                this is a html..你好
        </body>
</html>
```


## 3、常用标签

- 跑马灯marquee

```html
<marquee scrollamount="2"> 欢迎光临小王 </marquee>

```


```html
◎direction表示滚动的方向，值可以是left，right，up，down，默认为left
◎behavior表示滚动的方式，值可以是scroll（连续滚动）slide（滑动一次）alternate（往返滚动）
◎loop表示循环的次数，值是正整数，默认为无限循环
◎scrollamount表示运动速度，值是正整数，默认为6
◎scrolldelay表示停顿时间，值是正整数，默认为0，单位似乎是毫秒
◎align表示元素的垂直对齐方式，值可以是top，middle，bottom，默认为middle
◎bgcolor表示运动区域的背景色，值是16进制的RGB颜色，默认为白色
◎height、width表示运动区域的高度和宽度，值是正整数（单位是像素）或百分数，默认width=100% height为标签内元素的高度
◎hspace、vspace表示元素到区域边界的水平距离和垂直距离，值是正整数，单位是像素。
◎onmouseover=this.stop() onmouseout=this.start()表示当鼠标以上区域的时候滚动停止，当鼠标移开的时候又继续滚动。
```


#### 1、展示标签

- 标题标签：数组越大字体越小，加粗

```html
<h1>我是一号标题</h1> 
<h2>我是二号标题</h2>
<h3>我是三号标题</h3>
<h4>我是四号标题</h4> 
<h5>我是五号标题</h5>
<h6>我是六号标题</h6> 
注:从一号到六号标题字体从大到小
属性:align="left/center/right"用来描述文本的对其方式 居左(默认)/居中/居右 
<h6 align="center">我是六号标题</h6><!--居右的六号标题-->
```


- <hr />分割线标签

  <hr /> 标签在 HTML 页面中创建水平分割线。用于分割内容、文章的小节。

```HTML
<hr color="red" width="1080px" size="3px"/>
color:颜色：单词  #16进制表现形式
size:高度
width：线宽度 值：px像素点

补充说明：
块级元素：各占据一行，垂直方向排列。块级元素从新行开始结束接着一个断行。
width、height、padding、margin有效。
内联元素：在一条直线上排列，都是同一行的，水平方向排列。
设置width、height、padding、margin无效。
```


#### 2、换行字符

- <br/>

```html
王恒杰<br/>
这人 真好！！！
```


#### 2、特殊字符

- &nbsp;

```html
王恒杰<br/>
这人&nbsp;真好！！！
```


- &copy

```html
版权所有&copy;翻版必究！
```


#### 4、上下标

- 上标<sup></sup>

```html
A<sup>2</sup>
```


- 下标

```HTML
A<sub>3</sub>
```


#### 5、居中<center></center>

```html
<center></center>
```


#### 6、字体

- <font>标签

```java
1. font 标签是字体标签，它可以简单的修改文本样式效果。但现在已经不建议使用了。
size取值范围：1~7
```


```html
<font face="宋体" color="red" size="7" >我是字体标签</font>

<font color="red">字体</font>

```


#### 7、块div

- div标签

```html
<div>...</div>
```


```java
div是html中最灵活最重要的元素，
div就像一个容器，里面可以装很多内容。本身没有特殊的语义。

它是**块级元素** ，会占用网页的一行。

Div的主要作用：可以通过调整自己的样式来完成网页的复杂布局
```


- 它可以把一些独立的逻辑部分(如网页中独立的栏目版块)划分出来，如下图：

![](https://i.loli.net/2021/10/07/4lpWDafRhkKEBYN.png)

#### 8、段落标签

```html
<p>段落内容</p>
```


#### 9、加粗标签

```html
<b>天王盖地虎</b>
```


#### 10、超链接

1、 链接其他资源

```html
<a>超链接</a> 
href属性:值为跳转的网址 
target属性:指定资源打开位置
值使用_blank,通过新的网页打开href的网址
值使用_self,打开本窗口
<a href="url">Link text</a>
<a href="http://www.w3school.com.cn/">Visit W3School</a>

HTML 链接 - target 属性
<a href="https://www.baidu.com/" target="_blank">百度一下</a>

```


2、锚点

```html
1、 定义锚点（被跳转的点）
<a name="锚点名称"><a/>
2、定义跳转链接（跳转到指定锚点）
<a href="#锚点名"><a/>

<a name="top">顶部的点</a>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<h1>测试内容</h1>
<a href="#top">跳转到顶部</a>

```


![](https://i.loli.net/2021/10/07/gCzibMqyZGIB7aK.png)

#### 11、图像标签（<img>）和源属性（Src）

```Java
<img/>
 src属性:图片的路径 
 你需要使用源属性（src）。src 指 "source"。
 源属性的值是图像的 URL 地址。
 
 绝对路径:相对于计算机图片的位置 盘符:/文价夹名/文件名.后缀名 
 相对路径:相对于当前的项目所在的位置 文件夹名/文件名.后缀名 
 注:把图片放在项目下的img文件夹里 
 
 width属性:图片的宽度 单位:px 
 像素 height属性:图片高度 单位:px
 alt属性:网页预展示的内容

<img alt="“ src="" title=""/>
1. alt:图片无法显示时显示的描述性文字
2. src:图片的地址（或路径）。这里分为相对路径和绝对路径
3. width和height:设置图片的宽度和高度
4. (了解) title:鼠标放在图片上时显示的描述性文字

```


```html
<img src="img/杨.jpg" width="345" title="杨福君是我媳妇!" alt="图片正在加载"/>
```


#### 12、列表

1. 无序列表

```html
<ul>
   <li>列表项</li>
   <li>列表项</li>
</ul>

无序列表是一个项目的列表，此列项目使用粗体圆点（典型的小黑圆圈）进行标记。
无序列表始于 <ul> 标签。每个列表项始于 <li>。
列表项内部可以使用段落、换行符、图片、链接以及其他列表等等。

```


```html
家电：
<ul>
  <li>冰箱</li>
  <li>电视</li>
  <li>洗衣机</li>
</ul>

```


2. 有序列表

```html
有序列表
同样，有序列表也是一列项目，列表项目使用数字进行标记。
有序列表始于 <ol> 标签。每个列表项始于 <li> 标签。
```


```html
<ol>
<li>Coffee</li>
<li>Milk</li>
</ol>

结果：
1.Coffee
2.Milk

```


3. 定义列表

```html
自定义列表不仅仅是一列项目，而是项目及其注释的组合。

自定义列表以 <dl> 标签开始。
每个自定义列表项以 <dt> 开始。
每个自定义列表项的定义以 <dd> 开始。
```


```html
<dl>
<dt>Coffee</dt>
<dd>Black hot drink</dd>
<dt>Milk</dt>
<dd>White cold drink</dd>
</dl>

浏览器显示如下：
Coffee
Black hot drink
Milk
White cold drink
定义列表的列表项内部可以使用段落、换行符、图片、链接以及其他列表等等

```


#### 13、表格

```html
<!--表格-->
    <table>
      <!--表头-->
      <thead>
        <!--行-->
        <tr>
          <!--单元格-->
          <th>表头第一格</th>
          <th>表头第一二</th>
        </tr>
      </thead>
      <!--表体-->
      <tbody>
        <tr>
          <!--单元格-->
          <td>表体第一行第一格</td>
        </tr>
        <tr></tr>
      </tbody>
    </table> 
    rowspan属性:值合并单元格的个数 
    colspan属性:值合并单元格的个数 
    位置:写在要合并的单元格的最左上格
```


```java
1. <table>标记这是一个表格
2. <tr>表示表格的一行
3. <td>表示表格的一列
4.跨列合并单元格用 colspan 属性 横向合并使用
5.跨行合并单元格用 rowspan 属性 纵向合并使用
6常用属性
  * border ：设置表格边框
  * width：设置表格的宽度，单位px
  * height：设置表格的高度，单位px
  * align：设置表格的对齐方式
  * cellpading:内边距，设置单元格与单元格之间的边距
  * cellspace: 单元格与边界之间的边距
  * bgcolor:背景颜色
  * background="test.png":背景图片 
```


```html
<table border="1px" width="500" height="500" align="center" cellpadding="0" cellspacing="0" background="img/杨.jpg">
<!-- 行-->
  <tr align="center">
<!-- 单元格-->
    <td>1</td>
    <td>2</td>
    <td>3</td>
  </tr>
  <tr align="center">
    <!-- 单元格-->
    <td>4</td>
    <td>5</td>
    <td>6</td>
  <tr align="center">
    <!-- 单元格-->
    <td>7</td>
    <td>8</td>
    <td>9</td>
  </tr>
</table>
```


- 合并行&合并列区别

![](https://i.loli.net/2021/10/07/JQEvYVL9uMofXS2.png)

举例

![](https://i.loli.net/2021/10/07/NyHr1wYWhi8qgnc.png)

#### 14、表单【重点】

- 表单标签

```java
<!--
表单标签：
action:数据发送的目标位置 url 资源定位符
method:方法，数据的提交的方式 
*get（method的默认值）：地址栏明文传递 ？拼接数据  明文传递数据：不安全，数据传输量4K
*post：**协议体** 传递数据，将传递的数据封装成一个数据包，包携带在协议中 密文传递数据：安全，传输数据无限制，实际开发中，用post
-->
<form action="www.baidu.com" method=""> </form>
```


- 传递数据是以key-value形式传递，key:属性名 value:值

```html
<form>
     <!-- 隐藏文本框： -->
   <input type="hidden" name="随意"/>
      <!--文本框--> 
      <span>姓名:</span> 
      <!-- value:默认值 -->
      <input type="text" name="username" /> 
      <!-- 换行 -->
       <br />
       
      <!--密码框--> 
      <span>密码:</span> 
      <input type="password" name="password" /> <br />
      
      <!--单选按钮-->
      <span>性别:</span> 
      <input type="radio" name="sex" value="男" />男 
      <input type="radio" name="sex" value="女" />女
       <br />
       
      <!--复选框--> 
      <span>爱好:</span> 
      <input type="checkbox" name="hobby" value="吸烟" />吸烟 
      <input type="checkbox" name="hobby"value="喝酒" />喝酒
       <input type="checkbox" name="hobby" value="烫头" />烫头 
       <br />
       
      <!--下拉列表--> <span>籍贯:</span>
       <select name="city">
        <option value="北京">北京</option>
        <option>天津</option>
        <option>成都</option>
      </select> <br />
      
      <!--上传框--> 
      <span>上传头像:</span> 
      <input type="file" name="img" />
       <br />
       
      <!--时间框--> 
      <span>出生日期:</span> 
      <input type="datetime-local" name="date" /> 
      <br />
      
      <!--文本域-->
       <span>建议:</span> 
       <textarea cols="20" rows="5" name="advice">留下您宝贵的建议... </textarea> <br />
      
      <!--按钮-->
       <input type="submit" value="登录" />
       <input type="reset" value="重写" /> 
       <input type="button" value="注册" />
       <input type="submit" value="Submit">
    </form>
```


```html
<form action="/demo/demo_form.asp">
First name:<br>
<input type="text" name="firstname" value="Mickey">
<br>
Last name:<br>
<input type="text" name="lastname" value="Mouse">
<br><br>
<input type="submit" value="Submit">
</form> 
```


#### 15、转义字符

```java
当显示页面时，浏览器会移除源代码中多余的空格和空行。
所有连续的空格或空行都会被算作一个空格。
即，HTML 代码中的所有连续的空行（换行）也被显示为一个空格。

在 HTML 中不能使用小于号（<）和大于号（>），这是因为浏览器会误认为它们是标签。

如果想表示多个空格，需要使用如下的转义字符。
```


![](https://i.loli.net/2021/10/07/EDJ7BHYgmUVuNG1.png)

```html
说明1：如需显示小于号，我们必须这样写：&lt; 或 &#60;
说明2：使用实体名而不是数字的好处是，名称易于记忆。
不过坏处是，浏览器也许并不支持所有实体名称（对实体数字的支持却很好）。
```


#### 16、内联框架(了解)

```html
<iframe src="1.html" name=""></iframe>

```


```java
1. src:一个网页的地址
2. name: iframe的名字，当<a>标签的target属性值为iframe的name时，超链接的目标页面会在iframe中打开
```


#### 17、<span>标签

```html
<span>标签是是内联元素，没有语义的，
它的作用就是为了设置单独的样式用的。
有了它就可以对某段文字里的几个字单独设置样式了。
<span style="background-color: red">span测试</span>

```


![](https://i.loli.net/2021/10/07/cdIKM4PWlxhraz2.png)

# 五、文本标签总结

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title>文本标签</title>
        </head>
        <body>
                <!--文本标签-->
                <!--下面的文字就是普通的文本，文本编辑器中的任何效果：
                比如空格，换行 都不影响页面，
                        页面想要实现效果 必须通过标签来实现
                -->
                媒体：为人父母要不要“持证上岗”？
                媒体：为人父母，要不要“持证上岗”？
                媒体：为人父母，要不要“持证上岗”？
                媒体：为人父母，要不要“持证上岗”？
                <!--标题标签
                        h1-h6  字号逐渐变小，每个标题独占一行，自带换行效果
                        h7之后都属于无效标签，但是浏览器也不会报错，而是以普通文本的形式进行展现
                -->
                <h1>媒体：为人父母，要不要“持证上岗”？</h1>
                <h2>媒体：为人父母，要不要“持证上岗”？</h2>
                <h3>媒体：为人父母，要不要“持证上岗”？</h3>
                <h4>媒体：为人父母，要不要“持证上岗”？</h4>
                <h5>媒体：为人父母，要不要“持证上岗”？</h5>
                <h6>媒体：为人父母，要不要“持证上岗”？</h6>
                <h7>媒体：为人父母，要不要“持证上岗”？</h7>
                <h8>媒体：为人父母，要不要“持证上岗”？</h8>
                <!--横线标签
                        width:设置宽度
                                        300px ：固定宽度
                                        30%：页面宽度的百分比，会随着页面宽度的变化而变化
                        align：设置位置  left ,center,right    默认不写的话就是center居中效果
                -->
                <hr width="300px" align="center"/>
                <hr width="30%" align="center"/>
                
                <!--段落标签：
                        段落效果：段落中文字自动换行，段落和段落之间有空行
                -->
                <p>&nbsp;&nbsp;&nbsp;&nbsp;5月&emsp;26日，“建议父母持合格&lt;父母证&gt;上岗&copy;”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。</p>
                <p>5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。</p>
                <p>5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。5月26日，“建议父母持合格父母证上岗”冲上微博热搜，迅速引发热议。在正在召开的全国两会上，全国政协委员许洪玲建议在社区举办家长课堂，建立“家长教育指导工作室”。针对准备入小学的家长开展相关课程教育，颁发“合格父母”上岗证随学生档案入学。</p>
                
                <!--加粗倾斜下划线-->
                <b>加粗</b>
                <i>倾斜</i>
                <u>下划线</u>
                <i><u><b>加粗倾斜下划线</b></u></i>
                <!--一箭穿心-->
                <del>你好 你不好</del>
                
                <!--预编译标签：在页面上显示原样效果-->
                <pre>
public static void main(String[] args){
        System.out.println("hello msb....");
}
                </pre>
                
                <!--换行-->
                5月26日，“建议父母持合格父母证上岗”冲上微博<br />热搜，迅速引发热议。在正在召开的全国两会上，全国政
                
                <!--字体标签-->
                <font color="#397655" size="7" face="萝莉体 第二版">建议父母持合格父母证上岗</font>
                
        </body>
</html>

```


# 六、多媒体标签

```html

<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--图片
                        src:引入图片的位置
                                引入本地资源
                                引入网络资源
                        width:设置宽度
                        height:设置高度
                        注意:一般高度和宽度只设置一个即可，另一个会按照比例自动适应
                        title:鼠标悬浮在图片上的时候的提示语，默认情况下（没有设置alt属性） 图片如果加载失败那么提示语也是title的内容
                        alt:图片加载失败的提示语
                -->
                <img src="img/ss6.jpg" width="300px" title="这是一个美女小姐姐" alt="图片加载失败"/>
                <img src="https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1833909874,761626004&fm=26&gp=0.jpg" />
                <!--音频-->
                <embed src="music/我要你.mp3"></embed>
                <br />
                <!--视频-->
                <embed src="video/周杰伦 - 说好的幸福呢.mp4" width="500px" height="500px"></embed>
                <embed src="//player.video.iqiyi.com/38913f9ed7358c0933e82a03d9b26ec1/0/0/v_19rv8qeokk.swf-albumId=9194699400-tvId=9194699400-isPurchase=0-cnId=undefined" allowFullScreen="true" quality="high" width="480" height="350" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>
        </body>
</html>


```


# 七、超链接标签

![](https://i.loli.net/2021/10/07/sfVt4X6UeGYmhp2.png)

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--超链接标签：作用：实现页面的跳转功能
                        href:控制跳转的目标位置
                        target:_self 在自身页面打开 （默认效果也是在自身页面打开）    _blank 在空白页面打开
                --> 
                <a href="文本标签.html">这是一个超链接01</a><!--跳转到本地资源-->
                <a href="">这是一个超链接02</a> <!--跳转到自身页面-->
                <a href="abc">这是一个超链接03</a><!--跳转的目标找不到，提示找不到资源-->
                <a href="https://www.baidu.com" target="_self">这是一个超链接04</a><!--跳转到网络资源-->
                <a href="https://www.baidu.com" target="_blank">这是一个超链接05</a><!--跳转到网络资源-->
                
                <a href="https://www.baidu.com" target="_blank"><img src="img/ss.jpg" /></a>
        </body>
</html>


```


### 附加：设置锚点

设置锚点：
应用场合：当一个页面太长的时候，就需要设置锚点，然后可以在同一个页面的不同位置之间进行跳转。
同一个页面不同位置的跳转：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <a name="1F"></a>
                <h1>手机</h1>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <p>华为p40</p>
                <a name="2F"></a>
                <h1>化妆品</h1>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <p>大宝</p>
                <a name="3F"></a>
                <h1>母婴产品</h1>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <p>奶粉</p>
                <a name="4F"></a>
                <h1>图书</h1>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <p>thinking in java</p>
                <a href="#1F">手机</a>
                <a href="#2F">化妆品</a>
                <a href="#3F">母婴产品</a>
                <a href="#4F">书籍</a>
        </body>
</html>
```


# 八、列表标签总结

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--无序列表:
                        type:可以设置列表前图标的样式   type="square"
                        如果想要更换图标样式，需要借助css技术： style="list-style:url(img/act.jpg) ;"
                -->
                <h1>起床以后需要做的事</h1>
                <ul type="square">
                        <li>睁眼</li>
                        <li>穿衣服</li>
                        <li>上厕所</li>
                        <li>吃早饭</li>
                        <li>洗漱</li>
                        <li>出门</li>
                </ul>
                <!--有序列表:
                        type:可以设置列表的标号：1,a,A,i,I
                        start:设置起始标号
                -->
                <h1>学习java的顺序</h1>
                <ol type="A" start="3">
                        <li>JAVASE</li>
                        <li>ORACLE</li>
                        <li>MYSQL</li>
                        <li>HTML</li>
                        <li>CSS</li>
                        <li>JS</li>
                </ol>
                
        </body>
</html>
```


# 九、表格标签

应用场景：在页面布局很规整的时候，可能利用的就是表格。
合并原理：

![](https://i.loli.net/2021/10/07/aqN5iORlboGzJKm.png)

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--表格：4行4列
                        table:表格
                        tr:行
                        td:单元格
                        th:特殊单元格：表头效果：加粗，居中
                        默认情况下表格是没有边框的，通过属性来增加表框：
                        border:设置边框大小
                        cellspacing：设置单元格和边框之间的空隙
                        align="center"  设置居中
                        background 设置背景图片 background="img/ss.jpg"
                        bgcolor :设置背景颜色
                        rowspan:行合并
                        colspan：列合并
                -->
                <table border="1px" cellspacing="0px" width="400px" height="300px" bgcolor="darkseagreen" >
                        <tr bgcolor="bisque">
                                <th>学号</th>
                                <th>姓名</th>
                                <th>年纪</th>
                                <th>成绩</th>
                        </tr>
                        <tr>
                                <td align="center">1001</td>
                                <td>丽丽</td>
                                <td>19</td>
                                <td rowspan="3">90.5</td>
                        </tr>
                        <tr>
                                <td colspan="2" align="center">2006</td>
                                <td>30</td>
                        </tr>
                        <tr>
                                <td>3007</td>
                                <td>小明</td>
                                <td>18</td>
                        </tr>
                </table>
        </body>
</html>

```


效果：

![](https://i.loli.net/2021/10/07/3FdqEZhfBx4LWot.png)

# 十、框架

#### 1、内嵌框架

内嵌框架是用于在网页中嵌入一个网页并让它在网页中显示.
添加内嵌框架的语法:


```html
<iframe src=" URL "></iframe>
URL 指定独立网页的路径.
```


案例：
展示图书：

书籍展示首页：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <iframe src="书籍导航页面.html" height="700px" width="30%"></iframe>
                <!--内嵌框架-->
                <iframe name="iframe_my" width="67%" height="700px" src="img/think in java.jpg"></iframe>
        </body>
</html>
```


左侧导航页面：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <h1>我喜欢的图书展示</h1>
                <ul>
                        <li>
                                <a href="img/java核心技术.jpg" target="iframe_my">java核心技术</a>
                        </li>
                        <li>
                                <a href="img/think in java.jpg" target="iframe_my">think in java</a>
                        </li>
                        <li>
                                <a href="img/大话设计模式.jpg" target="iframe_my">大话设计模式</a>
                        </li>
                        <li>
                                <a href="img/深入理解java虚拟机.jpg" target="iframe_my">深入理解java虚拟机</a>
                        </li>
                        <li>
                                <a href="img/算法图解.jpg" target="iframe_my">算法图解</a>
                        </li>
                </ul>
        </body>
</html>

```


右侧书籍展示页面：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                这是展示列表
        </body>
</html>

```


#### 2、练习：邮箱

![](https://i.loli.net/2021/10/07/yBeinzMuUZmXL9Y.png)

![](https://i.loli.net/2021/10/07/RzEkeTm9r2uCXiK.png)

![](https://i.loli.net/2021/10/07/m1pd7vHrUP4klVE.png)

![](https://i.loli.net/2021/10/07/qkuMJ95BW7Iv4jw.png)

![](https://i.loli.net/2021/10/07/i7kOzFqtKLTpRgH.png)

![](https://i.loli.net/2021/10/07/ybQz5GBswiN1Pm7.png)

#### 3、框架集合

frameset 元素可定义一个框架集。它被用来组织多个窗口（框架）。每个框架存有独立的文档。在其最简单的应用中，frameset 元素仅仅会规定在框架集中存在多少列或多少行。您必须使用 cols 或 rows 属性。

里面如果只有一个框架用frame标签<br />如果多个框架用frameset标签<br />用cols 或 rows进行行，列的切割

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <!--框架集合：和body是并列的概念，不要将框架集合放入body中-->
        <frameset rows="20%,*,30%">
                <frame />
                <frameset cols="30%,40%,*">
                        <frame />
                        <frame src="index.html"/>
                        <frame />
                </frameset>
                <frameset cols="50%,*">
                        <frame />
                        <frame />
                </frameset>
        </frameset>
</html>

```


![](https://i.loli.net/2021/10/07/aZkQ1R7lLOYsg9e.png)


# 十一、form表单

前后端交互流程：

![](https://i.loli.net/2021/10/07/6TxLAnqfg9VXKji.png)

```HTML
表单在 Web 网页中用来给访问者填写信息，从而能采集客户端信息，使网页具有交互的功能。
一般是将表单设计在一个Html 文档中，当用户填写完信息后做提交(submit)操作，
于是表单的内容就从客户端的浏览器传送到服务器上，经过服务器上程序处理后，
再将用户所需信息传送回客户端的浏览器上，这样网页就具有了交互性。
这里我们只讲怎样使用Html 标志来设计表单。
所有的用户输入内容的地方都用表单来写，如登录注册、搜索框。
一个表单一般应该包含用户填写信息的输入框,提交按钮等，这些输入框,按钮叫做控件,
表单很像容器,它能够容纳各种各样的控件。
```


```html
<form action＝"url" method=get|post name="myform" ></form>
-name：表单提交时的名称
-action：提交到的地址
-method：提交方式，有get和post两种，默认为get
```


#### 1、模拟百度搜索

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title>百度一下，你就知道</title>
                <link rel="shortcut icon" href="https://www.baidu.com/favicon.ico" type="image/x-icon" />
        </head>
        <body>
                <form action="https://www.baidu.com/s" method="get">
                        <!--文本框-->
                        <input type="text" name="wd"/>
                        <!--提交按钮-->
                        <input type="submit" value="百度一下"/>
                </form>
        </body>
</html>
```


#### 2、表单元素

form表单中可以放入的标签 就是表单元素

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <form action="" method="get">
                        <!--表单元素-->
                        <!--文本框:
                                input标签使用很广泛，通过type属性的不同值，来表现不同的形态。
                                type="text"  文本框，里面文字可见
                                表单元素必须有一个属性：name 有了name才可以提交数据,才可以采集数据
                                然后提交的时候会以键值对的形式拼到一起。
                                value:就是文本框中的具体内容
                                键值对：name=value的形式
                                如果value提前写好，那么默认效果就是value中内容。
                                一般默认提示语：用placeholder属性，不会用value--》value只是文本框中的值。
                                
                                readonly只读：只是不能修改，但是其他操作都可以，可以正常提交
                                disabled禁用：完全不用，不能正常提交
                                
                                写法：
                                readonly="readonly"
                                readonly
                                readonly = "true"
                        -->
                        <input type="text" name="uname"  placeholder="请录入身份证信息"/>
                        <input type="text" name="uname2" value="123123" readonly="true"/>
                        <input type="text" name="uname3" value="456456" disabled="disabled"/>
                        <!--密码框:效果录入信息不可见-->
                        <input type="password" name="pwd"  />
                        <!--单选按钮：
                                注意：一组单选按钮，必须通过name属性来控制，让它们在一个分组中，然后在一个分组里只能选择一个
                                正常状态下，提交数据为：gender=on ，后台不能区分你提交的数据
                                不同的选项的value值要控制为不同，这样后台接收就可以区分了
                                
                                默认选中：checked="checked"
                        -->
                        性别：
                        <input type="radio" name="gender" value="1" checked="checked"/>男
                        <input type="radio" name="gender" value="0"/>女
                        
                        <!--多选按钮:
                                必须通过name属性来控制，让它们在一个分组中，然后在一个分组里可以选择多个
                                不同的选项的value值要控制为不同，这样后台接收就可以区分了
                                多个选项提交的时候，键值对用&符号进行拼接：例如下：
                                favlan=1&favlan=3
                        -->
                        你喜欢的语言：
                        <input type="checkbox" name="favlan" value="1" checked="checked"/>java
                        <input type="checkbox" name="favlan" value="2" checked="checked"/>python
                        <input type="checkbox" name="favlan" value="3"/>php
                        <input type="checkbox" name="favlan" value="4"/>c#
                        
                        <!--文件-->
                        <input type="file" />
                        <!--隐藏域-->
                        <input type="hidden" name="uname6" value="123123" />
                        <!--普通按钮：普通按钮没有什么效果，就是可以点击，以后学了js，可以加入事件-->
                        <input type="button" value="普通按钮" />
                        <!--特殊按钮：重置按钮将页面恢复到初始状态-->
                        <input type="reset" />
                        <!--特殊按钮：图片按钮-->
                        <img src="img/java核心技术.jpg" />
                        <input type="image" src="img/java核心技术.jpg" />
                        
                        
                        <!--下拉列表
                                默认选中：selected="selected"
                                多选：multiple="multiple"
                        -->
                        你喜欢的城市：
                        <select name="city" multiple="multiple">
                                <option value="0">---请选择---</option>
                                <option value="1">哈尔滨市</option>
                                <option value="2" selected="selected">青岛市</option>
                                <option value="3">郑州市</option>
                                <option value="4">西安市</option>
                                <option value="5">天津市</option>
                        </select>
                        
                        <!--多行文本框
                                利用css样式来控制大小不可变：style="resize: none;"
                        -->
                        自我介绍：
                        <textarea style="resize: none;" rows="10" cols="30">请在这里填写信息。。</textarea>
                        <br />
                        <!--label标签
                                一般会在想要获得焦点的标签上加入一个id属性，然后label中的for属性跟id配合使用。
                        -->
                        <label for="uname">用户名：</label><input type="text" name="uername" id="uname"/>
                        
                        <!--特殊按钮：提交按钮：具备提交功能-->
                        <input type="submit" />
                </form>
        </body>
</html>

```


#### 3、HTML5新增type类型

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <form action="" method="get">
                        <!--email:
                                html5的类型可以增加校验
                        -->
                        <input type="email" name="email" />
                        <!--url-->
                        <input type="url" />
                        <!--color-->
                        <input type="color" />
                        <!--number:
                                min:最小值
                                max:最大值
                                step:步长
                                value:默认值：一定在步长的范围中，否则不能提交
                        -->
                        <input type="number" min="1" max="10" step="3" value="4"/>
                        <!--range-->
                        1<input type="range" min="1" max="10" name="range" step="3"/>10
                        <!--date-->
                        <input type="date" />
                        <!--month-->
                        <input type="month" />
                        <!--week-->
                        <input type="week" />
                        <!--提交按钮-->
                        <input type="submit" />
                </form>
        </body>
</html>

```


#### 4、HTML5新增属性

```HTML
<!--
                                HTML5新增属性：
                                multiple：多选
                                placehoder:默认提示
                                autofocus:自动获取焦点
                                required:必填项
                        -->
                        <input type="text" autofocus="autofocus"/>
                        <input type="text" required="required" />
```


