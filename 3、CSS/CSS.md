# CSS

# 补充：MIME

```java
type:文件类型 text/css  

MIME类型：网络资源在互联网传输过程中的数据解析类型
 MIME类型表现：大类型/小类型
 
 text/css text/hyml 网页 text/plain:txt文本类型 
 image/jpj 图片类型 image/jpeg图片类型
```


# 一、什么是CSS

1、 专门对HTML网页美化操作

```Java
* HTML 从语义的角度描述页面的结构 负责内容展示. 
* CSS从审美的角度描述页面的样式 排版 设计 美化html.
```


![](https://i.loli.net/2021/10/07/4jKsxV9dkIguQN1.png)

![](https://i.loli.net/2021/10/07/8T2LwYHbsQMDeVP.jpg)

1. CSS(Cascading Stylesheet) 全称级联样式表，也称为层叠样式表。

2. 作用： CSS主要就是给HTML增加更加绚丽的风格和样式。

```java
层叠：样式的叠加
样式表：各种各样样式的集合
```


# 二、语法：

```java
* 语法: 配合HTML标签使用. 
* 核心语法:选中标签进行css修饰
 ① html标签 
 ② css代码
```


- CSS样式由选择符(选择器)和声明组成，而声明又由属性和值组成，如下图所示：

![](https://i.loli.net/2021/10/07/71HiUhsZlKAVxpT.png)

![](https://i.loli.net/2021/10/07/vfhpJ2dWcPOlZbK.png)

```html
格式为：

选择器 {

​      样式名：样式值;

​      样式名：样式值;
       …………
}
```


### 0、语法说明：

- 属性 (property) 是你希望改变的属性，并且每个属性都有一个值。属性和值被冒号分开，并由花括号包围，这样就组成了一个完整的样式声明（declaration）

```css
例如：p {color: blue}
```


- 多个声明：如果要定义不止一个声明，则**需要用英文分号”;”将每个声明分开** 。虽然最后一条声明的最后可以不加分号，但尽量在每条声明的末尾都加上分号

- 每行最好只描述一个属性

- **CSS对大小写不敏感，但建议使用小写。不过存在一个例外：class 和 id 名称对大小写是敏感的。** 

- CSS注释：**/** **注释内容** **/** 

### 1、常用样式--(属性)

```css
样式名:样式值; 
/*常用样式:*/ 
文字颜色： color: #ffffff; 
文字样式： font-style: italic;(斜体) 
字体大小： font-size:16px; 
文字粗细： font-weight: bold;(粗体) 
文字修饰： text-decoration:none;(无修饰线) 
对齐方式： text-align:right; (文字右对齐)
                    center;(内部元素居中)
                    justify;(文 字分散对齐) 
背景颜色： background-color:#ff0000; (红色背景) 
背景图片： background-image:url()
边框样式： border:1px solid #ff0000;(边框) 
列表样式： list-style:none;(去除列表样式)
```


### 2.1 颜色

- CSS中的颜色用RGB颜色：红色(red)、绿色(green)、蓝色(blue) ——光学三原色表示。

![](https://i.loli.net/2021/10/07/VCnPfhRWt74wcIe.jpg)

- RGB中每种颜色都用两位十六进制数表示，0表示没有光，F表示最强的光，并按红、绿、蓝的顺序排列，前面再加上#号。

- 例如：#000000黑色；#FFFFFF 白色；#FF0000红色；

&ensp;&ensp;&ensp;&ensp;#00FF00绿色；#0000FF蓝色等等。

- color：red；

- 颜色可以写颜色名如：black, blue, red, green, white, yellow等

- 颜色也可以写rgb值和十六进制表示值：如rgb(255,0,0)，#00F6DE，如果写十六进制值必须加#

### 2.2 宽度

width:19px;

宽度可以写像素值：19px；

也可以写百分比值：20%；

### 2.3 高度

height:20px;

同宽度一样

### 2.4 背景颜色

background-color:#0F2D4C

### 2.5 扩展

#### 2.5.1 字体样式

|字体颜色|color: *#bbffaa* ;|
|---|---|
|**字体大小** |**font-size: ** **20px** **;** |



#### 2.5.2 黑色1像素实线边框

```css
border: 1px solid black;
```


#### 2.5.3 DIV居中

```css
margin-left: auto;
margin-right: auto;
```


#### 2.5.4 文本居中

```css
text-align: center;
```


#### 2.5.5 超链接去下划线

```css
text-decoration: none;
```


#### 2.5.6 表格细线

```css
table {
  /*设置边框*/
  border: 1px solid black;
  /*将边框合并*/
  border-collapse: collapse;
}
  
td,th {
  /*设置边框*/
  border: 1px solid black;
}

```


#### .5.7 列表去除修饰

```css
ul {
  list-style: none;
}
```


### 2、选择器分类

选择器：浏览器根据“选择器”确定受CSS样式影响的HTML元素。

```css
选择器名{ 
样式名:样式值; 
样式名:样式值; 
... }
css中选择器非常多:常用的有 标签/通用/类/id/后代/子类/并集/交集...
 /*标签选择器*/--标签名{} 
 /*通用选择器*/--*{} 
 /*类选择器*/--.类名{} 
 /*id选择器*/--#类名{}
```


#### 分类1：标签选择器

![](https://i.loli.net/2021/10/07/vfhpJ2dWcPOlZbK.png)

按照标签名选中相应的元素。如上图的p。

```css
p {
  color:red;
}
```


#### 分类2：类选择器

按照元素的类名选中相应的元素，使用.class值

```css
.class属性值{

}
```


举例：

```css
<p class=”foot”>青春正好</p>
<b class=”foot”>编程趁早</b>


.foot {
  color:red;
}
```


#### 分类3：ID选择器

按照元素的id选中相应的元素，使用#id值

```css
#id属性值{

}
```


举例：

```css
<p id=”abc”>大家好</p>

#abc {
  color:red;
}

```


#### 分类4：组和选择器

可以同时使用多个选择器选中一组元素，使用，分隔不同的选择器。

```css
选择器1，选择器2，……，选择器N{
  color：red；
}
```


举例：

```css
#div1,.mini{
  background-color:red;
}
```


#### 分类5：派生选择器

根据上下文关系，选择元素的后代元素，使用空格隔开

```css
选择器1  选择器2 … {

  color:red;

}



```


```html
<style>
.ol li{
  color: red;
  background-color: yellow;
}
  </style>
  
  体部：
<ol class="ol">
  <div>王恒杰</div>
  <li>1人</li>
  <li>2人</li>
  <li>3人</li>
</ol>
```


**注意：id选择器>类选择器>标签选择器** 

#### 综合案例：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        /*
                         【1】基本选择器：元素选择器：
                         通过元素的名字进行定位，它会获取页面上所有这个元素，无论藏的多深都可以获取到
                         格式：
                         元素名字{
                           css样式;
                         }
                         * */
                        h1{
                                color: red;
                        }
                        i{
                                color: blue;
                        }
                        /*
                         【2】基本选择器：类选择器
                         应用场合：不同类型的标签使用相同的类型
                         格式：
                         .class的名字{
                           css样式;
                         }
                         */
                        .mycls{
                                color: green;
                        }
                        
                        /*
                         【3】基本选择器：id选择器：
                         应用场合：可以定位唯一的一个元素
                         不同的标签确实可以使用相同的id，但是一般我们会进行人为的控制，让id是可以唯一定位到一个元素。
                         格式：
                         #id名字{
                           css样式;
                         }
                         */
                        
                        #myid{
                                color: yellow;
                        }
                </style>
        </head>
        <body>
                <h1>我是<i>一个</i>标题</h1>
                <h1>我是一个标题</h1>
                <h1 class="mycls">我是一个标题</h1>
                <h1>我是一个标题</h1>
                <h2 class="mycls">我是h2标题</h2>
                <h2>我是h2标题</h2>
                <h2 id="myid">我是h2标题</h2>
        </body>
</html>
```


#### 分类6：关系选择器div&span

div和span 结合css用于页面的布局。div+css 用于页面布局。

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                /*
                 我们可以通俗的理解，把div理解为一个“塑料袋”
                 div属于块级元素--》换行
                 span属于行内元素--》没有换行效果
                 span:里面的内容占多大，span包裹的区域就多大*/
                        div{
                                border: 1px red solid;
                        }
                        span{
                                border: 1px greenyellow solid;
                        }
                </style>
        </head>
        <body>
                <div>马士兵马士兵<br />马士兵马士兵</div>
                <div>马士兵</div>
                <span>马士兵马士兵</span>
                <span>马士兵</span>
                <span>马士兵</span>
        </body>
</html>

```


- 关系选择器

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        /*关系选择器:
                         * 后代选择器：只要是这个元素的后代，样式都会发生变化
                         * div下面的所有h1标签样式都会改变
                         */
                        /*div h1{
                                color: red;
                        }*/
                        /*关系选择器：子代选择器
                         只改变子标签的样式*/
                        div>h1{
                                color: royalblue;
                        }
                        span>h1{
                                color: yellow;
                        }
                </style>
        </head>
        <body>
                <div>
                        <h1>这是标题</h1>
                        <h1>这是标题</h1>
                        <h1>这是标题</h1>
                        <h1>这是标题</h1>
                        <h1>这是标题</h1>
                        <span>
                                <h1>这是标题</h1>
                                <h1>这是标题</h1>
                                <h1>这是标题</h1>
                                <h1>这是标题</h1>
                                <h1>这是标题</h1>
                        </span>
                </div>
                        
        </body>
</html>

```


#### 分类7：属性选择器

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        /*属性选择器*/
                        input[type="password"]{
                                background-color: red;
                        }
                        input[type="text"][value="zhaoss1"]{
                                background-color: yellow;
                        }
                        
                </style>
        </head>
        <body>
                <form>
                        用户名：<input type="text" value="zhaoss1" />
                        用户名2：<input type="text" value="zhaoss2" />
                        密码：<input type="password" value="123123" />
                        <input type="submit" value="登录" />
                </form>
        </body>
</html>

```


#### 分类8：伪类选择器

- 伪类选择器  向某些选择器添加特殊效果。

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        .mycls:hover{
                                color: red;
                        }
                </style>
        </head>
        <body>
                <h1 class="mycls">我是标题</h1>
        </body>
</html>
```


- 一般伪类选择器都用在超链接上：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        /*设置静止状态*/
                        a:link{
                                color: yellow;
                        }
                        /*设置鼠标悬浮状态*/
                        a:hover{
                                color: red;
                        }
                        /*设置触发状态*/
                        a:active{
                                color: blue;
                        }
                        
                        /*设置完成状态*/
                        a:visited{
                                color: green;
                        }
                </style>
        </head>
        <body>
                <a href="index.html">超链接</a>
        </body>
</html>

```


#### 练习：百度导航栏

![](https://i.loli.net/2021/10/07/gy9os6VNvakmnUM.png)

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        ul{
                                list-style-type: none;/*将无序列表前面的图标取消*/
                        }
                        li{
                                float:left;/*向左浮动*/
                                margin-left: 20px;/*设置间隔20px*/
                        }
                        a{
                                text-decoration: none;/*去掉下划线*/
                                font-size: 13px;/*字号*/
                                color: black;/*字体颜色*/
                        }
                        a:hover{
                                color: #0000FF;
                        }
                        div{
                                /*定位：*/
                                position: absolute;/*绝对定位*/
                                right:200px;
                        }
                </style>
        </head>
        <body>
                <div>
                        <ul>
                                <li>
                                        <a href="aaaa">新闻</a>
                                </li>
                                <li>
                                        <a href="aaaa">hao123</a>
                                </li>
                                <li>
                                        <a href="aaaa">地图</a>
                                </li>
                                <li>
                                        <a href="aaaa">视频</a>
                                </li>
                        </ul>
                </div>
        </body>
</html>

```


### 3、书写位置

注意：**实际开发中三种书写方式用的最多的是：<br />第三种：外部样式：因为这种方式真正做到了  元素页面和样式 分离** 

- 三种书写方式优先级：
**就近原则** 

![](https://i.loli.net/2021/10/07/JxADXgdkBUS5z6y.png)

#### 1、内嵌式

```css
<标签名 style="样式1;样式2;...">标签体</标签名>
 <li style="color:red;background-color: darkgray;">

例：
<body>
    <p style="color: blue;">王恒杰</p>
    <ul style="color:blue;background-color: aqua;">
      <li>王恒杰</li>
      <li>杨福君</li>
    </ul>
 </body>

例二：
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--
                        书写方式：内联样式（行内样式）
                        在标签中加入一个style属性，CSS的样式作为属性值
                        多个属性值之间用;进行拼接
                -->
                <h1 style="color: deeppink;font-family: '宋体';">这是一个h1标题</h1>
        </body>
</html> 

**缺点：会冗余，更改复杂** 
<h1 style="color: red;background-color: green">下课！！！</h1>
<h1 style="color: red;background-color: green">想得美！！！</h1>
```


#### 2、内联样式

1. 在标签上加上需要使用的属性

```html
<标签名 属性名="属性值">标签体</标签名> 
<li class="test1">
```


2. 在标签里添加标签

```html
<style type="text/css"> 选择器 </style> 
/* 类选择器 */ 
.test1{ 
color: yellow; 
background-color: lightgray;
}
```


案例：

```html
头部：
<style>
      .test2{
        color: red;
        background-color: aqua;
      }
      .test1{
        color: chartreuse;
      }
    </style>

体部：
<!-- 外联样式 -->
    <p class="test1">王恒杰</p>
    <ul>
      <li class="test2">王恒杰</li>
      <li class="test2">杨福君</li>
    </ul>

```


#### 3、外部样式

1. 在标签上加上需要使用的属性

```html
<标签名 属性名="属性值">标签体</标签名> 
<li class="test2">
```


2. 新建.css文件(放在项目下的css文件中)

```css
选择器 
.test2{ 
 color: green;
 background-color: darkgray; 
 }
```


![](https://i.loli.net/2021/10/07/NBlAb5zGTUkSdaX.png)

3. 在标签里添加标签

```html
rel:描述引入文件和当前文件的关系，Stylesheet:样式表
type:文件类型 text/css  

MIME类型：网络资源在互联网传输过程中的数据解析类型
 MIME类型表现：大类型/小类型
 
 text/css text/hyml 网页 text/plain:txt文本类型 
 image/jpj 图片类型 image/jpeg图片类型
 
<link type="text/css" rel="stylesheet" href="css/文件名.css"/>
```


例子：

```css
css部：
.test1{
  color: #FF0000;
  background-color:yellow;
}
.test2{
  color: #00FFFF;
  background-color:green ;
}
```


```html

头部：
<link type="text/css" rel="stylesheet" href="css/test.css" />
体部：
<!-- 外部样式 -->
    <p class="test1">王恒杰</p>
    <ol>
      <li class="test2">王恒杰</li>
      <li class="test2">杨福君</li>
    </ol>
```


### 4、CSS样式编写位置

方式一：写在标签的style属性中：

```html
<p style=“font-size:30px”>字体大小用px表示</p>
```


```html
<p style="color: red ; font-size: 12px">落霞与孤鹜齐飞</p>
```


方式二：写在html头的style标签中(style标签一般写在head标签与title标签之间)：

```html
<style type="text/css">
  p{
    color:blue;
    background-color: yellow;
  }
</style>
```


方式三：写在外部的css文件中，然后通过link标签引入外部的css文件

```css
<link rel="stylesheet" type="text/css" href="style.css"> 
```


其中，style.css定义如下：

```css
@charset "UTF-8";
/* 这是css与html结合使用的第三种方式  */
div { 
  border: 1px solid red; 
} 
span {
  border: 1px solid green; 
}
```


> 说明：**当同一个 HTML 元素被不止一个样式定义时，会使用哪个样式呢？** 

优先级按照上述讲的三种方式依次降低。内联样式（在 HTML 元素内部）拥有最高的优先权。



# 三、浮动和定位和布局

## 1、浮动

```css
float:left;(左浮动)right;(右浮动)
```


### 【1】什么是浮动？

浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
CSS 的 Float（浮动）使元素脱离文档流，按照指定的方向（左或右发生移动），直到它的外边缘碰到包含框或另一个浮动框的边框为止。
说到脱离文档流要说一下什么是文档流，文档流是是文档中可显示对象在排列时所占用的位置/空间，而脱离文档流就是在页面中不占位置了。

### 【2】浮动初衷：文字环绕图片

![](https://i.loli.net/2021/10/07/Vf9QoRjsmDwkB4X.png)

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        img{
                                float: left;
                        }
                </style>
        </head>
        <body>
                <img src="img/java核心技术.jpg" />
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
                浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。浮动设计的初衷为了解决文字环绕图片问题，浮动后一定不会将文字挡住，这是设计初衷，不能违背的。
        </body>
</html>
```


### 【3】浮动原理

请看下图，当把框 1 向右浮动时，它脱离文档流并且向右移动，直到它的右边缘碰到包含框的右边缘：

![](https://i.loli.net/2021/10/07/aLUjyYSc7BtReZ4.png)

再请看下图，当框 1 向左浮动时，它脱离文档流并且向左移动，直到它的左边缘碰到包含框的左边缘。因为它不再处于文档流中，所以它不占据空间，实际上覆盖住了框 2，使框 2 从视图中消失。
如果把所有三个框都向左移动，那么框 1 向左浮动直到碰到包含框，另外两个框向左浮动直到碰到前一个浮动框。

![](https://i.loli.net/2021/10/07/1783MY9DZeFVNgb.png)

如下图所示，如果包含框太窄，无法容纳水平排列的三个浮动元素，那么其它浮动块向下移动，直到有足够的空间。如果浮动元素的高度不同，那么当它们向下移动时可能被其它浮动元素“卡住”：

![](https://i.loli.net/2021/10/07/mRAdawbOCnYiIL7.png)

### 【4】浮动的语法：

![](https://i.loli.net/2021/10/07/qFkSscR45xdbJiQ.png)

### 【5】利用代码感受浮动效果：

先设置一个大的div，然后里面放入三个小的div:

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--外层div-->
                <div style="background-color: pink;">
                        <div style="width: 100px;height: 100px;background-color: chartreuse;">11</div>
                        <div style="width: 200px;height: 200px;background-color: coral;">22</div>
                        <div style="width: 300px;height: 300px;background-color: yellow">33</div>
                </div>
        </body>
</html>
```


效果：（没有任何浮动）

![](https://i.loli.net/2021/10/07/84uol36IBQqrwzR.png)

然后先给绿色div加上浮动：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--外层div-->
                <div style="background-color: pink;">
                        <div id="div01" style="width: 100px;height: 100px;background-color: chartreuse;float: left;">11</div>
                        <div id="div02" style="width: 200px;height: 200px;background-color: coral;">22</div>
                        <div id="div03" style="width: 300px;height: 300px;background-color: yellow">33</div>
                </div>
        </body>
</html>
```


再给橙色div添加浮动：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--外层div-->
                <div style="background-color: pink;">
                        <div id="div01" style="width: 100px;height: 100px;background-color: chartreuse;float: left;">11</div>
                        <div id="div02" style="width: 200px;height: 200px;background-color: coral;float: left;">22</div>
                        <div id="div03" style="width: 300px;height: 300px;background-color: yellow">33</div>
                </div>
        </body>
</html>

```


效果

![](https://i.loli.net/2021/10/07/iLwbKzAYpHMCDBv.png)

再给黄色div设置浮动：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--外层div-->
                <div style="background-color: pink;">
                        <div id="div01" style="width: 100px;height: 100px;background-color: chartreuse;float: left;">11</div>
                        <div id="div02" style="width: 200px;height: 200px;background-color: coral;float: left;">22</div>
                        <div id="div03" style="width: 300px;height: 300px;background-color: yellow;float: left;">33</div>
                </div>
        </body>
</html>
```


![](https://i.loli.net/2021/10/07/hWOA5i2MxlbgnZD.png)

现在在三个div下面再加上一个紫色div：·

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--外层div-->
                <div style="background-color: pink;">
                        <div id="div01" style="width: 100px;height: 100px;background-color: chartreuse;float: left;">11</div>
                        <div id="div02" style="width: 200px;height: 200px;background-color: coral;float: left;">22</div>
                        <div id="div03" style="width: 300px;height: 300px;background-color: yellow;float: left;">33</div>
                </div>
                <div style="width: 500px;height: 500px;background-color: blueviolet;"></div>
        </body>
</html>
```


![](https://i.loli.net/2021/10/07/vLPiZbsEJC4AfID.png)

用浮动要考虑影响，看看是否对其他的元素有影响。

### 【6】消除浮动影响：


方式1：给浮动的父节点加入一个属性overflow:hidden

![](https://i.loli.net/2021/10/07/X2NFIDmpoTYVMQy.png)

方式2：
给父节点加一个高度，让粉色div“撑起来”

![](https://i.loli.net/2021/10/07/8yhYIdUQVBAOiuR.png)

方式3：
被影响的元素紫色div：给它加入一个属性。

![](https://i.loli.net/2021/10/07/3zM6f8CFdNTRil4.png)

## 2、定位

#### 【1】position 属性指定了元素的定位类型。

![](https://i.loli.net/2021/10/07/LcrKyoJh7QDFvAI.png)

#### 【2】静态定位：（static）

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
       <!--静态定位：
           如果我们不写position属性的话，相当于默认效果就是静态定位。
           静态效果：就是元素出现在它本该出现的位置。一般使用静态定位可以直接省略不写。
         -->
                <img src="img/java核心技术.jpg" style="position: static;"/>
        </body>
</html>
```


#### 【3】相对定位：（relative）

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--相对定位：
                        相对元素自身所在的原来的位置进行定位。
                        可以设置 left,right,top,bottom四个属性
                        效果：在进行相对定位以后，元素原来所在的位置被保留了，被占用了--》保留站位其他元素的位置不会发生移动
                        一般情况下，left和right不会同时使用  ,选择一个方向即可.top和bottom不会同时使用,选择一个方向即可
                        优先级：左上>右下
                -->
                <div style="width: 500px;height: 500px;background-color: pink;">
                        <div style="width: 100px;height: 100px;background-color: bisque;"></div>
                        <div style="width: 100px;height: 100px;background-color: yellow;position: relative;bottom: 10px;right: 20px;"></div>
                        <div style="width: 100px;height: 100px;background-color: green;"></div>
                </div>
        </body>
</html>
```


相对定位的应用场合：
（1）元素在小范围移动的时候
（2）结合绝对定位使用

再说一个属性：z-index
设置堆叠顺序，设置元素谁在上谁在下。
注意：z-index属性要设置在定位的元素上，拥有更高堆叠顺序的元素总是会处于堆叠顺序较低的元素的前面

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
        </head>
        <body>
                <!--相对定位：
                        相对元素自身所在的原来的位置进行定位。
                        可以设置 left,right,top,bottom四个属性
                        效果：在进行相对定位以后，元素原来所在的位置被保留了，被占用了--》保留站位其他元素的位置不会发生移动
                        一般情况下，left和right不会同时使用  ,选择一个方向即可.top和bottom不会同时使用,选择一个方向即可
                        优先级：左上>右下
                -->
                <div style="width: 500px;height: 500px;background-color: pink;">
                        <div style="width: 100px;height: 100px;background-color: bisque;position: relative;left: 10px;z-index: 10;"></div>
                        <div style="width: 100px;height: 100px;background-color: yellow;position: relative;bottom: 10px;right: 20px;z-index: 90;"></div>
                        <div style="width: 100px;height: 100px;background-color: green;"></div>
                </div>
        </body>
</html>
```


#### 【4】绝对定位：（absolute）

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        #outer{
                                width: 500px;
                                height: 500px;
                                background-color: pink;
                                margin-left:300px;
                        }
                        #div01{
                                width: 100px;
                                height: 100px;
                                background-color: cornflowerblue;
                                position: absolute;
                                left: 30px;
                                top: 50px;
                        }
                        #div02{
                                width: 100px;
                                height: 100px;
                                background-color: coral;
                        }
                </style>
        </head>
        <body>
                <div id="outer">
                        <div id="div01">111</div>
                        <div id="div02">222</div>
                </div>
        </body>
</html>
```


![](https://i.loli.net/2021/10/07/5MV9mbnXY8dDhuJ.png)

暂时来说看到的效果：蓝色div相对body产生的位移，相对body进行位置的改变，然后蓝色div发生位移以后，原位置得到了释放。橙色div移动上去了！
实际开发中，我们往往让蓝色div在粉色div中发生位移效果：
配合定位来使用：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        #outer{
                                width: 500px;
                                height: 500px;
                                background-color: pink;
                                margin-left:300px;
                                position: relative;/*直接设置一个相对定位*/
                        }
                        #div01{
                                width: 100px;
                                height: 100px;
                                background-color: cornflowerblue;
                                position: absolute;
                                left: 30px;
                                top: 50px;
                        }
                        #div02{
                                width: 100px;
                                height: 100px;
                                background-color: coral;
                        }
                </style>
        </head>
        <body>
                <div id="outer">
                        <div id="div01">111</div>
                        <div id="div02">222</div>
                </div>
        </body>
</html>
```


![](https://i.loli.net/2021/10/07/Rs1FfBLKOQjaudb.png)

总结：
当给一个元素设置了绝对定位的时候，它相对谁变化呢？它会向上一层一层的找父级节点是否有定位，如果直到找到body了也没有定位，那么就相对body进行变化，如果父级节点有定位（绝对定位，相对定位，固定定位），但是一般我们会配合使用父级为相对定位，当前元素为绝对定位，这样这个元素就会相对父级位置产生变化。无论是上面的哪一种，都会释放原来的位置，然后其他元素会占用那个位置。


- **开发中建议使用：父级节点relative定位，子级节点使用绝对定位。** 

#### 【5】固定定位：（fixed）


应用场合：在页面过长的时候，将某个元素固定在浏览器的某个位置上，当拉动滚动条的时候，这个元素位置不动。
代码：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        #mydiv{
                                width: 50px;
                                height: 400px;
                                background-color: cadetblue;
                                /*固定定位*/
                                position: fixed;
                                right: 0px;
                                top: 300px;
                        }
                </style>
        </head>
        <body>
                <div id="mydiv"></div>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
        </body>
</html>
```


## 3、盒子

```css
行级标签:显示内容在一行内,不满不换行 img a input span
块级标签:显示内容在页面占独立区域,内容多少,都换行 p hn li div 
div盒子:内容+内边距+边框+外边框
```


#### 【1】生活案例入手：

![](https://i.loli.net/2021/10/07/GFstJrcnH1yQMDY.png)

#### 【2】盒子模型：

页面上也有很多元素，元素之间的布局/设计 依靠 盒子模型：
所有HTML元素可以看作盒子，在CSS中，"box model"这一术语是用来设计和布局时使用。
CSS盒模型本质上是一个盒子，封装周围的HTML元素，它包括：边距，边框，填充，和实际内容。
盒模型允许我们在其它元素和周围元素边框之间的空间放置元素。
下面的图片说明了盒子模型(Box Model)：

![](https://i.loli.net/2021/10/07/dnAoXxaTuBcDpil.png)

![](https://i.loli.net/2021/10/07/MDdrcns7X4h25fI.png)

![](https://i.loli.net/2021/10/07/pvRhjPT1rVJMoI6.png)

- 边框

```html
border: 线型 线宽 颜色;边宽
double:双实线
dotted:点线
solid:实线

```


- padding 内边框 内补白：当前元素与内部元素之间的距离

```java
padding-top/right/bottom/lrft:10px


```


- margin 外边距 外补白：当前元素与外部元素之间的距离

![](https://i.loli.net/2021/10/07/kbCKR9H7I2gYFGs.png)

#### 【3】在浏览器端验证一下盒子模型：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        div{
                                width: 100px;
                                height: 100px;
                                background-color: yellowgreen;
                                margin-left: 100px;
                                border: 4px red solid;
                        }
                </style>
        </head>
        <body>
                <div>我是div</div>
        </body>
</html>
```


![](https://i.loli.net/2021/10/07/MTPSnLhgsI71YR5.png)

【4】写代码感受盒子模型：

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                /*将所有元素的样式：外边距，边框，内边距全部设置为0*/
                        *{
                                margin: 0px;
                                border: 0px;
                                padding: 0px;
                        }
                        #outer{
                                width: 440px;
                                height: 450px;
                                background-color: lightskyblue;
                                margin-left: 100px;
                                margin-top: 100px;
                                padding-top: 50px;
                                padding-left: 60px;
                        }
                        #mydiv{
                                width: 170px;
                                height: 150px;
                                background-color: pink;
                                padding-top: 50px;
                                padding-left: 30px;
                        }
                </style>
        </head>
        <body>
                <div id="outer">
                        <div id="mydiv">我是div</div>
                </div>
        </body>
</html>

```


![](https://i.loli.net/2021/10/07/SGCx7sfpi9NcYI3.png)

练习：

![](https://i.loli.net/2021/10/07/zrB5WU4CXJ9KLmk.png)

```html
<!DOCTYPE html>
<html>
        <head>
                <meta charset="UTF-8">
                <title></title>
                <style type="text/css">
                        #mydiv{
                                width: 65px;
                                height: 60px;
                                background: url(img/小喇叭.png) no-repeat 13px 12px;/*不平铺*/
                                background-color: #EFEFEF;/*注意：要先写背景图 再写背景色*/
                                /*设置div中文字效果*/
                                font-size: 17px;/*字体大小*/
                                font-family: "微软雅黑";/*字体类型*/
                                color: #666666;/*字体颜色*/
                                padding-top: 50px;/*盒子模型设置内边距*/
                                text-align: center;/*设置水平居中*/
                                /*设置固定定位*/
                                position: fixed;
                                right: 0px;
                                top: 200px;
                        }
                </style>
        </head>
        <body>
                <div id="mydiv">
                        最新<br />发布
                </div>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
                <p>你好</p>
        </body>
</html>

```



# 四、FontAwesome(css框架)

- 简介: 图标库

- 下载地址: [https://fontawesome.dashgame.com/](https://fontawesome.dashgame.com/)

- 语法:

```html
引用font图标库的css样式文件
 1. 下载压缩包,解压 
 2. 将负责font图标库中css目录和fonts目录粘贴在项目根目录下. 
 3. 在<head>标签中添加标签
 
  <link rel="stylesheet" type="text/css" href="css/font- awesome.min.css"/> 
  4.fontAwesome预先定义了样式,通过类选择实现 
  <i class="fa fa-xxxx"></i> 
  <i class="fa fa-xxx fa-spin">
```


- 实例代码

```html
<i class="fa fa-support"></i>
<i class="fa fa-mobile"></i> 
<i class="fa fa-subway"></i> 
<i class="fa fa-spinner fa-spin"></i>
```


![](https://i.loli.net/2021/10/07/1pEthw7KovViRDL.png)

# 五、 BootStrap(css框架)

- 简介: facebook提供css框架

- 语法:

```html
引用bootstrap框架css文件 
1. 下载压缩包,解压 
2. 将解压后的 css/fonts/js 目录粘贴在项目根目录下. 
3. 在<head>标签中添加标签 
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
 4. 使用css样式效果. 
 <标签名 class="bootstrap内置class名字"></标签名>
```



#### 1、文本

```html
# 文字颜色
<span class="text-primary">文字</span> primary 深蓝
<span class="text-info">文字</span>  info 淡蓝 
<span class="text-danger">文字</span> danger 红色
<span class="text-warning">文字</span> warning 黄色
<span class="text-success">文字</span> success 绿色 
<span class="text-default">文字</span> # 背景色
<span class="label label-danger">上架</span>
```


#### 2、按钮

```html
# html标签: 
a 超链接 
button 按钮 
submit 提交 
# bootstrap样式 
btn 设置为bootstrap按钮 
btn-xxx 设置按钮颜色
btn-xxx 设置按钮大小 
btn-lg 大 
btn-sm 小 
btn-xs 迷你 
# 示例
<a href="#" class="btn btn-primary btn-lg">超链接</a> 
<button class="btn btn-info btn-sm">普通按钮</button>
<input type="submit" class="btn btn-success btn-xs" value="注 册">
```


#### 3、图片

```html
# HTML标签 
<img src="img/macbook.jpg" width="100px" height="100"/>
 # bootstrap样式 
 img-thumbnail 
 img-circle 
 img-rounded
```


#### 4、table效果

```html
# html标签
 <table border="1" width="60%"> 
 <thead> 
 <tr>
 <th>商品名</th> 
 <th>价格</th>
 <th>库存</th> 
 <th>描述</th> 
 </tr> 
 </thead>
 <tbody>
 <tr>
   <td>XPS15</td>
   <td>100</td>
   <td>99</td>
   <td></td>
 </tr> 
 <tr>
   <td>thinkpad x230</td> 
   <td>200</td> 
   <td>199</td> 
   <td></td> 
 </tr> 
</tbody> 
</table> 
# bootstrap样式 
table标签的样式 
table 表格效果 
table-bordered 表格边框 
table-hover 数据行鼠标移入背景加深 
tr标签样式: 
bg-primary 设置背景色
```


# 六、项目：当当图书网

![](https://i.loli.net/2021/10/07/SbeNg4xEKD9wRW5.png)

validate：生效

相关属性：

```css
1、clear:none
图像的左侧和右侧均不允许出现浮动元素：
img{
  float:left;
  clear:both;
  }
clear 属性规定元素的哪一侧不允许其他浮动元素。

值        描述
left     在左侧不允许浮动元素。
right    在右侧不允许浮动元素。
both     在左右两侧均不允许浮动元素。
none     默认值。允许浮动元素出现在两侧。
inherit  规定应该从父元素继承 clear 属性的值。
2、
```


