# XML&Jsoup解析器

- 目标

```Java
* XML
  1. 概念
  2. 语法
  3. 解析
```


# 一、XML：

```Java
1. 概念：Extensible Markup Language **可扩展标记语言** 
  * 可扩展：标签都是自定义的。 <user>  <student>

  * 功能
    * 存储数据
      1. 配置文件
      2. 在网络中传输
  * xml与html的区别
    1. xml（可扩展标记语言）标签都是自定义的，html（超文本标记语言）标签是预定义。
        *与properties竞争
    2. xml的语法严格，html语法松散
    3. xml是存储数据的，html是展示数据

  * w3c:万维网联盟，**是xml和html共同的爹** 

2. 语法：
  * 基本语法：
    1. xml文档的后缀名 .xml
    2. xml第一行必须定义为文档声明
    3. xml文档中有且仅有一个根标签
    4. 属性值必须使用引号(单双都可)引起来
    5. 标签必须正确关闭
    6. xml标签名称区分大小写

```


- 快速入门

```xml
 <?xml version='1.0' econding="utf-8"?>//不允许有空行
    <users>//Xml必须要有根标签
      <user id='1'>//属性值必须有引号引起来id='1 '
        <name>zhangsan</name>
        <age>23</age>
        <gender>male</gender>
        <br/>
      </user>
      
      <user id='2'>
        <name>lisi</name>
        <age>24</age>
        <gender>female</gender>
      </user>
    </users>
```


# 二、组成部分

```java
* 组成部分：
    1. 文档声明
      1. 格式：<?xml 属性列表 ?>
      2. 属性列表：
        * version：版本号，必须的属性（不写version是要报错的）
        * encoding：编码方式。告知解析引擎当前文档使用的字符集，默认值：ISO-8859-1
        * standalone：是否独立
          * 取值：
            * yes：不依赖其他文件
            * no：依赖其他文件
    2. 指令(了解)：结合css的
      * <?xml-stylesheet type="text/css" href="a.css" ?>
    3. 标签：标签名称自定义的
      * 规则：
        * 名称可以包含字母、数字以及其他的字符 
        * 名称不能以数字或者标点符号开始 
        * 名称不能以字母 xml（或者 XML、Xml 等等）开始 
        * 名称不能包含空格 

    4. 属性：
      id属性值唯一
    5. 文本：
      * CDATA区：在该区域中的数据会被原样展示
        * 格式：  <![CDATA[ 数据 ]]>

```


- CDADA区展示该区域的数据

![](https://i.loli.net/2021/10/07/y3mhgUnKZpOe7FQ.png)

# 三、XML的约束

```java
* 约束：规定xml文档的书写规则
    * 作为框架的使用者(程序员)：
      1. 能够在xml中引入约束文档
      2. 能够简单的读懂约束文档
    
    * 分类：
      1. DTD:一种简单的约束技术
      2. Schema:一种复杂的约束技术
```


- 谁写框架，谁写xml的约束文档

![](C:%5CUsers%5C86151%5CDesktop%5CXML&Jsoup%E8%A7%A3%E6%9E%90%E5%99%A8%5Cimage%5C%E7%BA%A6%E6%9D%9F.bmp)

# 四、约束文档的分类

### 1、dtd文档约束【了解 】

```jaav
  * DTD：
      * 引入dtd文档到xml文档中
        * 内部dtd：将约束规则定义在xml文档中
        * 外部dtd：将约束的规则定义在外部的dtd文件中
          * 本地：<!DOCTYPE 根标签名 SYSTEM "dtd文件的位置">
          * 网络：<!DOCTYPE 根标签名 PUBLIC "dtd文件名字" "dtd文件的位置URL">
```


- 写框架的人写的dtd约束文档：

```XML
<!ELEMENT students (student*) ><!-- student+表示必须写里面的东西-->
<!ELEMENT student (name,age,sex)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT age (#PCDATA)>
<!ELEMENT sex (#PCDATA)>
<!ATTLIST student number ID #REQUIRED>
```


- 程序员使用dtd的约束文档

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE students SYSTEM "student.dtd">

<students>
  <student number="itcast_0001">
    <name>tom</name>
    <age>18</age>
    <sex>male</sex>
  </student>
  
</students>
```


- dtd约束文档的缺点

```Java
1. 不能限定内容
1. 语法结构：
DTD的语法与XMl不同，使用DOM，XPath，XSL无法处理，为自动化文档处理带来不便
2. 数据类型
DTD数据类型不能自由扩充，不利于XML数据交换场合验证
3.文档结构
DTD中，所有元素、属性都是全局的， 无法声明仅与上下文位置相关的元素或属性
4. 名称空间
DTD中没有名称空间的概念，不直接支持名称空间

```


### 2、Schema约束文档【主流】

```java
  * Schema:
      * 引入：
        1.填写xml文档的根元素
        2.引入xsi前缀.  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        3.引入xsd文件命名空间.  xsi:schemaLocation="http://www.itcast.cn/xml  student.xsd"
        4.为每一个xsd约束声明一个前缀,作为标识  xmlns="http://www.itcast.cn/xml" 

      <students   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.itcast.cn/xml"
        xsi:schemaLocation="http://www.itcast.cn/xml  student.xsd">
```


- student.xsd

```xml
<?xml version="1.0"?>
<xsd:schema xmlns="http://www.itcast.cn/xml"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        targetNamespace="http://www.itcast.cn/xml" elementFormDefault="qualified">
    <xsd:element name="students" type="studentsType"/>
    <xsd:complexType name="studentsType">
        <xsd:sequence>
            <xsd:element name="student" type="studentType" minOccurs="0" maxOccurs="unbounded"/>
        </xsd:sequence>
    </xsd:complexType>
    <xsd:complexType name="studentType">
        <xsd:sequence>
            <xsd:element name="name" type="xsd:string"/>
            <xsd:element name="age" type="ageType" />
            <xsd:element name="sex" type="sexType" />
        </xsd:sequence>
        <xsd:attribute name="number" type="numberType" use="required"/>
    </xsd:complexType>
    <xsd:simpleType name="sexType">
        <xsd:restriction base="xsd:string">
            <xsd:enumeration value="male"/>
            <xsd:enumeration value="female"/>
        </xsd:restriction>
    </xsd:simpleType>
    <xsd:simpleType name="ageType">
        <xsd:restriction base="xsd:integer">
            <xsd:minInclusive value="0"/>
            <xsd:maxInclusive value="256"/>
        </xsd:restriction>
    </xsd:simpleType>
    <xsd:simpleType name="numberType">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value="heima_\d{4}"/>
        </xsd:restriction>
    </xsd:simpleType>
</xsd:schema> 

```


- xml导入写框架人写的约束

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
  1.填写xml文档的根元素
  2.引入xsi前缀.  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  3.引入xsd文件命名空间.  xsi:schemaLocation="http://www.itcast.cn/xml  student.xsd"
  4.为每一个xsd约束声明一个前缀,作为标识  xmlns="http://www.itcast.cn/xml" 
  
  
 -->
 <students   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.itcast.cn/xml" 
        xsi:schemaLocation="http://www.itcast.cn/xml  student.xsd"
         >
   <student number="heima_0001">
     <name>tom</name>
     <age>18</age>
     <sex>male</sex>
   </student>
     
 </students>
```


- Springmvc的xml详解

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns:context="http://www.springframework.org/schema/context"
    xmlns:mvc="http://www.springframework.org/schema/mvc"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context 
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc.xsd">

    
    <context:annotation-config />

   
    <context:component-scan base-package="cn.cisol.mvcdemo">
        <context:include-filter type="annotation"
            expression="org.springframework.stereotype.Controller" />
    </context:component-scan>

   
    <mvc:annotation-driven />

    
    <mvc:resources mapping="/resources/**" location="/resources/" />


    
    <bean
        class="org.springframework.web.servlet.view.ContentNegotiatingViewResolver">
        <property name="order" value="1" />
        <property name="mediaTypes">
            <map>
                <entry key="json" value="application/json" />
                <entry key="xml" value="application/xml" />
                <entry key="htm" value="text/html" />
            </map>
        </property>

        <property name="defaultViews">
            <list>
                
                <bean
                    class="org.springframework.web.servlet.view.json.MappingJackson2JsonView">
                </bean>
            </list>
        </property>
        <property name="ignoreAcceptHeader" value="true" />
    </bean>

    <bean
        class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass"
            value="org.springframework.web.servlet.view.JstlView" />
        <property name="prefix" value="/WEB-INF/jsps/" />
        <property name="suffix" value=".jsp" />
    </bean>


  
    <bean id="multipartResolver"
        class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
        <property name="maxUploadSize" value="209715200" />
        <property name="defaultEncoding" value="UTF-8" />
        <property name="resolveLazily" value="true" />
    </bean>

</beans>
```


# 五、XML的解析

### 1、解析xml文档

```jaav
3.解析：操作xml文档，将文档中的数据读取到内存中
  * 操作xml文档
    1. 解析(读取)：将文档中的数据读取到内存中
    2. 写入：将内存中的数据保存到xml文档中。**持久化的存储** 

```


### 2、xml常见的解析器【面试题】

```jaav
 * 解析xml的方式：
    1. DOM：将**标记语言文档一次性加载进内存** ，在内存中形成一颗dom树
      * 优点：操作方便，可以对文档进行CRUD的所有操作
      * 缺点：占内存，消耗内存
      用途：内存比较大的设备
    2. SAX：逐行读取，基于事件驱动的。
      * 优点：不占内存。
      * 缺点：只能读取，不能增删改
     用途：手机，音响 ，逐行读取，事件驱动的
```


- dom解析方式【重点，服务器端开发】

![](https://i.loli.net/2021/10/07/6aCsX8A4o7SdUuZ.png)

- **SAX逐行解析** ，事件驱动的

![](https://i.loli.net/2021/10/07/cMv16njYKRTrbVz.png)

```java
  * xml常见的解析器：
    1. JAXP：sun公司提供的解析器，支持dom和sax两种思想
    2. DOM4J：一款非常优秀的解析器
    3. Jsoup：jsoup 是一款Java 的HTML解析器，可直接解析某个URL地址、HTML文本内容。
         它提供了一套非常省力的API，
         可通过DOM，CSS以及类似于jQuery的操作方法来取出和操作数据。
    4. PULL：Android操作系统内置的解析器，sax方式的。
```


### 3、Jsoup解析器

- jsoup.jar包

[jsoup-1.11.2.jar](file/jsoup-1.11.2.jar)

```jaav
  * Jsoup：jsoup 是一款Java 的HTML解析器，
           可直接解析某个URL地址、HTML文本内容。
           它提供了一套非常省力的API，可通过DOM，
           CSS以及类似于jQuery的操作方法来取出和操作数据。
    * 快速入门：
      * 步骤：
        1. 导入jar包
        2. 获取Document对象
        3. 获取对应的标签Element对象
        4. 获取数据
  

 
```


- 案例代码：

```jaav
    //2.1获取student.xml的path
          String path = JsoupDemo1.class.getClassLoader().getResource("student.xml").getPath();
          //2.2解析xml文档，加载文档进内存，获取dom树--->Document
          Document document = Jsoup.parse(new File(path), "utf-8");
          //3.获取元素对象 Element
          Elements elements = document.getElementsByTag("name");
  
          System.out.println(elements.size());
          //3.1获取第一个name的Element对象
          Element element = elements.get(0);
          //3.2获取数据
          String name = element.text();
          System.out.println(name);
```


![](https://i.loli.net/2021/10/07/2hwrngD3poYjZd8.png)

- Jsoup所解析的代码

```xml
<?xml version="1.0" encoding="UTF-8" ?>

 <students   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.itcast.cn/xml" 
        xsi:schemaLocation="http://www.itcast.cn/xml  ../web/student.xsd"
         >
   <student number="heima_0001">
     <name>tom</name>
     <age>18</age>
     <sex>male</sex>
   </student>
  <student number="heima_0002">
    <name>whj</name>
    <age>21</age>
    <sex>male</sex>
  </student>
 </students>
```


### 4、Jsoup对象的使用

- soup,Document,Elements,Element,Node对象的使用：

#### 1. Jsoup：

```java
1. Jsoup：工具类，可以解析html或xml文档，返回Document
* parse：解析html或xml文档，返回Document
* parse​(File in, String charsetName)：解析xml或html文件的。
* parse​(String html)：解析xml或html字符串
* parse​(URL url, int timeoutMillis)：通过网络路径获取指定的html或xml的文档对象
```


1. parse​(File in, String charsetName)：解析xml或html文件的

![](https://i.loli.net/2021/10/07/YIJUjsHNz54w8rg.png)

2. parse​(String html)：解析xml或html字符串

![](https://i.loli.net/2021/10/07/cvq6bC8XGgIQjF5.png)

3. parse​(URL url, int timeoutMillis)：通过网络路径获取指定的html或xml的文档对象

```java
url:统一资源定位符
timeoutMillis:超时时间 10000=10秒

 URL url = new URL("https://www.baidu.com");//代表网络中的一个资源
        Document parse = Jsoup.parse(url, 10000);
        System.out.println(parse);

```


![](https://i.loli.net/2021/10/07/iD4QF2SNRu596ZV.png)

#### 2. Document：

```jaav
  2. Document：文档对象。代表内存中的dom树
    * 获取Element对象                                                                                                                                          
    * getElementById​(String id)：根据id属性值获取唯一的element对象
    * getElementsByTag​(String tagName)：根据标签名称获取元素对象集合
    * getElementsByAttribute​(String key)：根据属性名称获取元素对象集合
    * getElementsByAttributeValue​(String key, String value)：
                 根据对应的属性名和属性值获取元素对象集合
```



- 代码案例

![](https://i.loli.net/2021/10/07/pMX1tOzgJsAh4Rb.png)

```java
    public static void main(String[] args) throws IOException {
//        //2.获取Document对象，根据xml文档来获取
//        //2.1获取xml的path路径
    String path = JsoupDemo1.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
        //获取所有student
        Elements elements = document.getElementsByTag("student");
        System.out.println(elements);
    }

   打印结果：
   打印了student对象 

  <student number="heima_0001"> 
 <name>
  tom
 </name> 
 <age>
  18
 </age> 
 <sex>
  male
 </sex> 
</student>
<student number="heima_0002"> 
 <name>
  whj
 </name> 
 <age>
  21
 </age> 
 <sex>
  male
 </sex> 
</student>

//获取属性为id的元素对象们
    public static void main(String[] args) throws IOException {
//        //2.获取Document对象，根据xml文档来获取
//        //2.1获取xml的path路径
    String path = JsoupDemo1.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
//        获取属性id的对象们
        Elements id = document.getElementsByAttribute("id");
        System.out.println(id);
    }
结果：
<name id="2">
 tom
</name>



    public static void main(String[] args) throws IOException {
//        //2.获取Document对象，根据xml文档来获取
//        //2.1获取xml的path路径
    String path = JsoupDemo1.class.getClassLoader().getResource("student.xml").getPath();
        Document document = Jsoup.parse(new File(path), "utf-8");
//    获取number属性值为heima_0001的元素对象
        Elements value = document.getElementsByAttributeValue("number", "heima_0001");
        System.out.println(value);
    }
 

结果：
<student number="heima_0001"> 
 <name id="2">
  tom
 </name> 
 <age>
  18
 </age> 
 <sex>
  male
 </sex> 
</student> 
```


#### 3. Elements：

```java
 3. Elements：元素Element对象的集合。
              可以当做 ArrayList<Element>来使用
```


#### 4. Element：

```jaav
    4. Element：元素对象
      1. 获取子元素对象
        * getElementById​(String id)：根据id属性值获取唯一的element对象
        * getElementsByTag​(String tagName)：根据标签名称获取元素对象集合
        * getElementsByAttribute​(String key)：根据属性名称获取元素对象集合
        * getElementsByAttributeValue​(String key, String value)：
        根据对应的属性名和属性值获取元素对象集合

      2. 获取属性值
        * String attr(String key)：根据属性名称获取属性值
      3. 获取文本内容
        * String text():获取文本内容
        * String html():获取标签体的所有内容(包括字标签的字符串内容)
```


![](https://i.loli.net/2021/10/07/xkKaCVpd65TU7Rg.png)

#### 5. Node：

```Java
 5. Node：节点对象
      * 是Document和Element的父类
```


# 五、快捷查询方式：

#### 1、 selector:选择器

```java
  * 快捷查询方式：
    1. selector:选择器
      * 使用的方法：Elements  select​(String cssQuery)
        * 语法：参考Selector类中定义的语法
  
```



![](https://i.loli.net/2021/10/07/RIU5hWixV1Nj4Bn.png)

- 查询name标签

![](https://i.loli.net/2021/10/07/189YHxZlFq4Sn6o.png)

- 查询id为itcast的元素

![](https://i.loli.net/2021/10/07/cPvmQVs3fjuZRO9.png)

- 获取student标签并且number属性值为heima_0001

![](https://i.loli.net/2021/10/07/Cp1VQTvgkm2jZEY.png)

![](https://i.loli.net/2021/10/07/hH6I4fUXwmTOpqy.png)

#### 2、XPath

[JsoupXpath-0.3.2.jar](file/JsoupXpath-0.3.2.jar)

```jaav
 2. XPath：XPath即为XML路径语言，
             它是一种用来确定XML（标准通用标记语言的子集）文档中某部分位置的语言
      * 使用Jsoup的Xpath需要额外导入jar包。
      * 查询w3cshool参考手册，使用xpath的语法完成查询
      * 代码：
        //1.获取student.xml的path
            String path = JsoupDemo6.class.getClassLoader().getResource("student.xml").getPath();
            //2.获取Document对象
            Document document = Jsoup.parse(new File(path), "utf-8");
    
            //3.根据document对象，创建JXDocument对象
            JXDocument jxDocument = new JXDocument(document);
    
            //4.结合xpath语法查询
            //4.1查询所有student标签
            List<JXNode> jxNodes = jxDocument.selN("//student");
            for (JXNode jxNode : jxNodes) {
                System.out.println(jxNode);
            }
    
            System.out.println("--------------------");
    
            //4.2查询所有student标签下的name标签
            List<JXNode> jxNodes2 = jxDocument.selN("//student/name");
            for (JXNode jxNode : jxNodes2) {
                System.out.println(jxNode);
            }
    
            System.out.println("--------------------");
    
            //4.3查询student标签下带有id属性的name标签
            List<JXNode> jxNodes3 = jxDocument.selN("//student/name[@id]");
            for (JXNode jxNode : jxNodes3) {
                System.out.println(jxNode);
            }
            System.out.println("--------------------");
            //4.4查询student标签下带有id属性的name标签 并且id属性值为itcast
    
            List<JXNode> jxNodes4 = jxDocument.selN("//student/name[@id='itcast']");
            for (JXNode jxNode : jxNodes4) {
                System.out.println(jxNode);
            }
```


